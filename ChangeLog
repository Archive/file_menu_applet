2002-06-23  Christopher James Lahey  <clahey@ximian.com>

	* src/q-multi-menu.c (rebuild): Made the maximum be maximum
	instead of one less than maximum.  Made a-` never appear and makes
	fewer empty menus.

2002-06-21  Christopher James Lahey  <clahey@ximian.com>

	* src/q-multi-menu.c, src/q-multi-menu.h
	(q_multi_menu_list_nth_datum, q_multi_menu_append_with_data,
	q_multi_menu_set_maximum, q_multi_menu_freeze, q_multi_menu_thaw):
	Added these functions.

2002-06-19  Benjamin Kahn  <xkahn@ximian.com>

	* src/q-multi-menu.c (q_multi_menu_init): Set idle_id to zero so
	it won't try and remove good idle handlers.

2002-06-07  Benjamin Kahn  <xkahn@ximian.com>

	* src/flist-mime-icon.c (flist_get_thumbnail_image): Check for images in 
	user's home directory as well.  Lots of pointer stuff I should be 
	checking for in this function now.  Major FIXME.

	*  (Index): Update Version #.

	* src/main.c (pixmap_menu_item_new): Set usize on menu item so they all
	appear the same size.

	* src/flist-mime-icon.c (flist_get_thumbnail_image): Find nautilus
	thumbnail icon and see if it exists.
	(flist_mime_icon_load): Scale icons so they don't overlap menu label.

	* src/flist-mime-icon.h (ICON_SIZE): Move ICON_SIZE to header.

2002-06-01  Benjamin Kahn  <xkahn@ximian.com>

	* src/popup.c (create_dir_menu): Hook up the signal handler to be
	able to create a new directory.

	* src/fileop.c (create_dir): Add the ability to create a new directory.
	(create_dir_dialog): Add the UI for creating the new directory.

	* src/Makefile.am (noinst_HEADERS): Need title-item.h to compile...

2002-05-30  Benjamin Kahn  <xkahn@ximian.com>

	* src/main.c (show_directory_menu): When using fast icon lookups, use 
	file extensions since it takes no time at all.
	
	* src/main.c (show_directory_menu): Fix the reentrant problem AGAIN.

2002-05-29  Benjamin Kahn  <xkahn@ximian.com>

	* src/main.c (show_directory_menu): Fix a reentrant problem in
	menu generation where a menu could be generated twice.

2002-05-29  Christopher James Lahey  <clahey@ximian.com>

	* src/main.c, src/main.h (show_directory_menu): Set up a timer.
	Do a gtk_main_iteration loop after creating the directory menu.
	(load_directory_contents): Only reposition menu and do a
	gtk_main_iteration loop if a tenth of a second has passed since
	the last time you did one.

	* src/q-multi-menu.c (q_multi_menu_append): Added a debugging
	printf here.

2002-05-28  Christopher James Lahey  <clahey@ximian.com>

	* src/q-multi-menu.c, src/q-multi-menu.h (rebuild): Iterate over
	all the items instead of skipping the first.  Initialize
	menu->priv->lists[i]->items[j] to eaze debugging.  Cleaned up the
	code a bit.
	(maybe_rebuild): Made it so that if you call a query function with
	a queued rebuild, it rebuilds immediately.
	(q_multi_menu_append): Turned down the priority on our idle
	function.

2002-05-28  Christopher James Lahey  <clahey@ximian.com>

	* src/main.c: Make this print out more information if you turn on
	the qmultimenu stuff.

	* src/q-multi-menu.c, src/q-multi-menu.h: Added
	q_multi_menu_set_prefix.  Fixed a bunch of crashes and
	miscalculations.

2002-05-28  Benjamin Kahn  <xkahn@ximian.com>

	* src/flist-mime-icon.c (flist_mime_icon_load): Cache icon misses.  This
	speeds up the case where no icon can be found.

2002-05-28  Christopher James Lahey  <clahey@ximian.com>

	* acconfig.h: #undef GNOME_DATADIR

	* configure.in: Define GNOME_DATADIR.

	* src/flist-mime-icon.c (flist_nautilus_theme_load): Added check
	for GNOME_DATADIR/share/pixmaps/nautilus/.  Fixed checks in
	gnome_paths directories.

	* src/flist-mime-icon.h: Added flist_nautilus_theme_load and
	flist_mime_get_icon.

	* src/main.c (show_directory_menu): Comment out the unused
	variable here.

	* src/popup.c: #include "flist-mime-icon.h".
	(maybe_popup_menu): Removed unused variable here.

2002-05-28  Benjamin Kahn  <xkahn@ximian.com>

	* src/fileop.c (start_app_in_dir): 
	* src/popup.c (create_mime_in_dir): Allow starting an application in a 
	particular directory.  This is used for the right click menu on 
	directories.

	* src/flist-mime-icon.c (flist_nautilus_theme_load): Search the GNOME_PATH
	for the nautilus theme directory.

2002-05-23  Benjamin Kahn  <xkahn@ximian.com>

	* src/main.c (create_new_directory_menu): Move sub-menu creation into
	it's own function.  This makes the code a little easier to read.

2002-05-22  Benjamin Kahn  <xkahn@ximian.com>

	* src/popup.c (create_document_menu): 
	* src/main.c (do_truncate): Make directory titles and filenames
	in the right click menu truncate their names just like regular
	menu items do.

	* src/popup.c (maybe_popup_menu): 
	(create_document_menu): Pass the menuitem here and not just 
	the path.  This enables the delete menu item hack I just put in.
	it also makes a little more sense.

	* src/fileop.c (delete_file): When deleting a file, hide it's 
	menu item and DON'T pop down the menu.  FIXME: Should move
	to trash and report an error if there is a problem.

	* src/main.c (create_new_file_menuitem): 
	(create_new_directory_submenu): Add a parent value to the menuitem
	which points to it's parent menu.

2002-05-19  Benjamin Kahn  <xkahn@ximian.com>

	* src/flist-mime-icon.c (flist_mime_icon_get_file): Allow nautilus theme
	directory to be NULL without warnings.
	(flist_mime_get_icon): Fix missing "/" from path name.

2002-05-18  Benjamin Kahn  <xkahn@ximian.com>

	* src/flist-mime-icon.c (flist_mime_icon_get_file): For default icons
	check the nautilus theme.

	* src/popup.c (create_dir_menu): 
	* src/main.c (exec_fm_wrap): Open a Nautilus window here menu
	item added to directory right-click menu.

	* src/main.c (show_directory_menu): Reverse the icon logic so
	it loads icons when the user says.

	* src/flist-mime-icon.c (flist_mime_icon_get_file): Fall back to
	ICONPATH/nautilus if the icon isn't found.

	* Makefile.am: Fix Icon Path

	* src/main.h: Add Nautilus theme to config

	* src/main.c (main): Get Nautilus theme from gconf.

	* src/flist-mime-icon.c (flist_mime_get_icon): Use Nautilus Theme if found.

2002-05-16  Christopher James Lahey  <clahey@ximian.com>

	* src/Makefile.am: Add q-multi-menu.c and q-multi-menu.h.

	* src/q-multi-menu.c, src/q-multi-menu.h: New class.  For
	splitting one menu into many.

2002-05-15  Benjamin Kahn  <xkahn@ximian.com>

	* src/popup.c (create_dir_menu): Open a terminal here menu item.

	* src/main.c (exec_terminal_wrap): Wrapper function for running a terminal
	in an event handler.

	* src/fileop.c (exec_application): Allow cmd and cmd args to be nothing.
	(exec_terminal): Just run a terminal with nothing else in it.

2002-05-15  Christopher James Lahey  <clahey@ximian.com>

	* src/main.c (do_truncate): Don't add ... if truncate <= 3.

2002-05-15  Christopher James Lahey  <clahey@ximian.com>

	* src/main.c (do_truncate): Added this function to do the actual
	truncation.  Made it mid truncate.
	(pixmap_menu_item_new): Use do_truncate.

2002-05-15  Benjamin Kahn  <xkahn@ximian.com>

	* src/main.c (create_new_directory_submenu): 
	(create_new_file_menuitem): 
	* src/scroll-menu.c (compare_menu_items): Add a type to sorting.
	This allows directories to be sorted before files.

	* src/scroll-menu.c: Include string.h

2002-05-15  Christopher James Lahey  <clahey@ximian.com>

	* .cvsignore: Added aclocal.m4 and config.h.in.

	* aclocal.m4, config.h.in: Removed these autogenerated files from
	cvs.

	* src/Makefile.am (noinst_HEADERS): Added fileop.h

	* src/dnd.c (drag_end_menu_cb): Commented out this unused
	function.
	(target_drag_data_received): gtk_signal_emit takes a GTK_OBJECT().
	(popup_menu): Removed an unused variable.

	* src/fileop.c: Include fileop.h.  Moved exec_application to
	fileop.h.
	(destroy_status_popup2): Made this function take a gpointer to
	match the required signature for gtk_timeout_add.
	(exec_application, show_copy_dialog, show_move_dialog,
	show_rename_dialog, file_copy_sel, file_move_sel, file_rename_sel,
	delete_file, show_handler, set_status_on_popup): Fixed casts in
	these functions.
	(delete_file): Removed an unused variable here.

	* src/fileop.h: New header.

	* src/main.c: Include fileop.h and flist-mime-icon.h.
	(pixmap_menu_item_new): Cast to (GdkPixbuf *) here.  Fixed up the
	text handling here so we're not writing on a const char *.
	(exec_document_wrap): Fixed a cast here.
	(show_directory_menu, main): Removed unused variables in these
	functions.

	* src/main.h: Removed restore_grabs prototype since it's static
	now.

	* src/popup.c (create_dir_menu): Made this function static and
	added a prototype at the top.
	(create_document_menu): Removed a cast here to fix a warning.

	* src/preferences.c (setup_entry_config): #if 0ed out some unused
	code here.
	(set_menu_title): Removed an unused variable here.  Added some
	casts.
	(prefs_dialog_apply, create_prefs_dialog): Removed unused
	variables in these functions.

	* src/scroll-menu.c (compare_menu_items): Changed this function to
	match the signature required by g_list_insert_sorted.
	(scroll_menu_insert_sorted): Fixed a cast here.

2002-05-15  Benjamin Kahn  <xkahn@ximian.com>

	* src/popup.c (maybe_popup_dir_menu): (create_dir_menu): 
	* src/main.c (show_directory_menu): Finish the title menu
	for directories, importing the panel's title_item class
	and create a popup menu for it.  I've added a fake directory
	item for creating a terminal in that spot.

	* src/popup.c (restore_grabs): Fix Restore Grabs so it
	actually works.  I was passing focus to the wrong widget
	which seemed to work at first, but later caused problems.

2002-05-09  Benjamin Kahn  <xkahn@zoned.net>

	* src/main.c (create_new_directory_submenu): Removed a 
	double creation of the menu tearoff object.
	(show_directory_menu): Start adding directory titles.

2002-03-27  Benjamin Kahn  <xkahn@ximian.com>

	* configure.in: Bump version number.

	* src/main.h: src/main.c: src/dnd.c: src/fileop.c:
	src/preferences.c: Try and remove as many global 
	variables as possible.  Move them to a global struct.
	In the future, this struct will be passed around
	and acted upon.

2002-02-05  Benjamin Kahn  <xkahn@ximian.com>

	* src/preferences.c: Make preferences work.  (Lookup 
	real settings in dialog.) 
	Regularize preferences.
	Change menu caching to icon lookup setting.

	* src/main.c: Regularize preferences.
	Allow Fast operation. (get rid of fancy icons)	

	* src/fileop.c (translate_directory_name): Allow support
	for URI schemes that gnome-vfs doesn't support: ~/ ~user/ //
	and empty directories.
	(exec_application): Get rid of menu on application launch.

	* src/dnd.c: Cleanup, spelling mistakes.  Better debugging output.
	Get rid of menu on dnd drop.

	* src/Makefile.am (INCLUDES): Add GDK_PIXBUF_CFLAGS seperately.

	* configure.in (GNOME_GLADE_CFLAGS): Fix Typo

2002-02-04  Benjamin Kahn  <xkahn@ximian.com>

	* src/dnd.c (popup_menu): Don't grab focus to the submenu.  This allows us
	to drop on directories.  Nice!

2002-01-06  Benjamin Kahn  <xkahn@ximian.com>

	* configure.in (GNOME_VFS_REQUIRED): We are File Menu Applet, not Evolution.

2002-01-01  Christian Meyer  <chrisime@gnome.org>

	* file_menu_applet.desktop: Added de translation.

2001-12-31  Benjamin Kahn  <xkahn@ximian.com>

	* configure.in: take out parsing the macros directory... Hmm...

	* file_menu_applet.desktop: Add pt_BR translation, and change the 
	name of the Menu Item.  Also fix the Type and Exec line.

	* src/main.c (main): src/preferences.c (cancel_button_callback):
	Change Default Menu Name to Home Directory based on suggestions
	from the Debian Packager.

2001-12-30  Benjamin Kahn  <xkahn@ximian.com>

	* configure.in (GNOME_GLADE_CFLAGS), src/Makefile.am (INCLUDES): 
	Added glade include files.  Oops!

2001-12-28  Benjamin Kahn  <xkahn@ximian.com>

	* configure.in: Bump Version #.

	* src/fileop.c: Added this file to actually perform the file
	operations like copy, move, rename, and delete.

	* src/popup.c: Remove printfs. Add "Open With..." Sub
	Menu. Connect the right click menu to the file operations they
	claimed to perform.

	* src/main.c: Fix a seg fault. Split menu creation function into
	seperate functions to make code easier to read.  Allow menus to be
	truncated in certain characters.  Make the application exec stuff
	more general.  Migrate to GNOME-VFS from normal file operations.
	Add tooltips for long file names.  Allow the watched directory to
	change without needing a restart.

	* src/fma_prefs.glade: Add a rename file to... dialog.

	* src/Makefile.am: Fix the includes so pixmaps are found.

	* Makefile.am: Fix gnorba and desktop files so they are included
	in distcheck.

2001-09-17  Benjamin Kahn  <xkahn@ximian.com>

	* src/popup.c: Fixed a bug where documents without application
	handlers would segfault the applet if you right-clicked on
	them.  Also fixed the menu to say: add handler rather than
	edit handlers.  

2001-09-09  Benjamin Kahn  <xkahn@ximian.com>

	* Added file sorting and preferences work somewhat.

2001-07-25  Benjamin Kahn  <xkahn@cybersites.com>

	* Split main.c into multiple files for easier reading.  Support
	preliminary drag and drop.  This doesn't work for Nautilus yet.
	
2001-07-15  Benjamin Kahn  <xkahn@cybersites.com>

	* src/preferences.c: Added a preferences option to the applet
	right click menu.  It's not hooked up yet.

	* src/main.c: Added the beginnings of Drag n Drop support as
	requested by clahey.  Also, made the applet update the event loop
	while building the menu.  This results in a more snappy feeling.
	It also results in a neat looking animation the first time the
	applet is displayed.

	* src/main.h: Added new prototypes for the preferences menu and
	drag n drop.

