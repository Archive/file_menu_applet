/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <gnome.h>
#include <applet-widget.h>
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>
#include "flist-mime-icon.h"

#include "main.h"
#include "scroll-menu.h"

static GtkWidget *create_dir_menu (char *file_path);

mime_in_dir *create_mime_in_dir (char *mime, char *dir);

mime_in_dir *
create_mime_in_dir (char *mime, char *dir)
{
	mime_in_dir *mid;

	mid = g_new0 (mime_in_dir, 1);

	mid->mime_type = g_strdup (mime);
	mid->dir = dir;

	return mid;

}

/*most of this function stolen from the real gtk_menu_popup*/
static void
restore_grabs(GtkWidget *w, gpointer data)
{
	/*	GtkWidget *menu_item = data;*/
	GtkMenu *menu = GTK_MENU(data); 
	GtkWidget *xgrab_shell;
	GtkWidget *parent;
	/* Find the last viewable ancestor, and make an X grab on it
	 */
	parent = GTK_WIDGET (menu);
	xgrab_shell = NULL;
	while (parent) {
		gboolean viewable = TRUE;
		GtkWidget *tmp = parent;

		while (tmp) {
			if (!GTK_WIDGET_MAPPED (tmp)) {
				viewable = FALSE;
				break;
			}
			tmp = tmp->parent;
		}

		if (viewable)
			xgrab_shell = parent;

		parent = GTK_MENU_SHELL (parent)->parent_menu_shell;
	}

	/*only grab if this HAD a grab before*/
	if (xgrab_shell && (GTK_MENU_SHELL (xgrab_shell)->have_xgrab)) {
		GdkCursor *cursor = gdk_cursor_new (GDK_ARROW);

		GTK_MENU_SHELL (xgrab_shell)->have_xgrab = 
			(gdk_pointer_grab (xgrab_shell->window, TRUE,
					   GDK_BUTTON_PRESS_MASK |
					   GDK_BUTTON_RELEASE_MASK |
					   GDK_ENTER_NOTIFY_MASK |
					   GDK_LEAVE_NOTIFY_MASK,
					   NULL, cursor, 0) == 0);
		gdk_cursor_destroy (cursor);
	}
	
	gtk_grab_add (GTK_WIDGET (menu));
}


gboolean
maybe_popup_dir_menu (GtkWidget *widget, GdkEventButton *event, void *parent_menu)
{
	GtkWidget *dir_popup;
	gchar *dir_path;

	dir_path = (gchar *) gtk_object_get_data (GTK_OBJECT (widget), "full_path");
	
	if (event->button == 3) {
		dir_popup = create_dir_menu (dir_path);
		gtk_signal_connect(GTK_OBJECT(dir_popup),"deactivate",
				GTK_SIGNAL_FUNC(restore_grabs),parent_menu);
		gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button_press_event");
		gtk_menu_popup (GTK_MENU (dir_popup), 
				NULL, NULL, NULL, NULL,
				event->button, event->time);
		return TRUE;		
	}
	
	return FALSE;
}

gboolean
maybe_popup_menu (GtkWidget *widget, GdkEventButton *event, void *parent_menu)
{
	GtkWidget *document_popup;

	if (event->button == 3) {
		document_popup = create_document_menu (widget);
		gtk_signal_connect(GTK_OBJECT(document_popup),"deactivate",
				GTK_SIGNAL_FUNC(restore_grabs),parent_menu);
		gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button_press_event");
		gtk_menu_popup (GTK_MENU (document_popup), 
				NULL, NULL, NULL, NULL,
				event->button, event->time);
		return TRUE;
	} 
	
	return FALSE;
}

static GtkWidget *
create_dir_menu (char *file_path)
{
	GtkWidget *menuitem;
	GtkWidget *dir_popup;
	GtkWidget *new_menu;

	dir_popup = scroll_menu_new ();

	menuitem = pixmap_menu_item_new ("Open Terminal Here", NULL, 0);
	gtk_menu_append (GTK_MENU (dir_popup), menuitem);
	
	gtk_object_set_data (GTK_OBJECT (menuitem), "full_path", (gpointer) file_path);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (exec_terminal_wrap), (gpointer) file_path);	
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Open Nautilus Window Here", NULL, 0);
	gtk_menu_append (GTK_MENU (dir_popup), menuitem);

	gtk_object_set_data (GTK_OBJECT (menuitem), "full_path", (gpointer) file_path);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (exec_fm_wrap), (gpointer) file_path);	
	gtk_widget_show (menuitem);
	
	menuitem = pixmap_menu_item_new ("Create New...", NULL, 0);
	gtk_menu_append (GTK_MENU (dir_popup), menuitem);
	gtk_widget_show (menuitem);

	new_menu = scroll_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), new_menu);

	menuitem = pixmap_menu_item_new ("Text File", 
					 flist_mime_icon_load (file_path, 
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "text/plain"),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("text/plain", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Directory",
					 flist_mime_icon_load (file_path, 
							       GNOME_VFS_FILE_TYPE_DIRECTORY, 
							       NULL),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (create_dir_dialog), 
			    (gpointer) file_path);	
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Image File",
					 flist_mime_icon_load (file_path,
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "image/png"),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("image/png", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Spreadsheet",
					 flist_mime_icon_load (file_path,
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "application/vnd.ms-excel"),					 
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("application/vnd.ms-excel", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Document", 
					 flist_mime_icon_load (file_path,
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "application/msword"),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("application/msword", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("Presentation", 
					 flist_mime_icon_load (file_path,
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "application/vnd.ms-powerpoint"),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("application/vnd.ms-powerpoint", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new ("HTML Page",
					 flist_mime_icon_load (file_path,
							       GNOME_VFS_FILE_TYPE_UNKNOWN, 
							       "text/html"),
					 0);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (start_app_in_dir), 
			    (gpointer) create_mime_in_dir ("text/html", file_path));
	gtk_menu_append (GTK_MENU (new_menu), menuitem);
	gtk_widget_show (menuitem);

	return dir_popup;
	
}

GtkWidget * 
create_document_menu (GtkWidget *pmenu) 
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *file_info;
	GnomeVFSMimeApplication *application;
	gchar *file_path, *file_path_display;

	GtkWidget *menuitem;
	GtkWidget *document_popup;

	GList *applications;

	char *file_name, *file_name_display;

	file_path = (gchar *) gtk_object_get_data (GTK_OBJECT (pmenu), "full_path");

	if (strlen (file_path) > 40) {
		file_path_display = do_truncate (file_path, 40);
	} else {
		file_path_display = file_path;
	}

	document_popup = scroll_menu_new ();

	/* Detect the file type. */
	
	file_info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info ((char *) file_path, file_info, (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
								 | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
	if (result != GNOME_VFS_OK) {
		fprintf (stderr, "file_menu_applet: %s: %s\n",
			 file_path, gnome_vfs_result_to_string (result));
		return NULL;
	}
	
	/* Find application */
	application = gnome_vfs_mime_get_default_application ((char *)file_info->mime_type);

	file_name = strrchr (file_path, '/');
	if (file_name)
		file_name += 1;
	else
		file_name = file_path;

	if (strlen (file_name) > 40) {
		file_name_display = do_truncate (file_name, 40);
	} else {
		file_name_display = file_name;
	}
	
	if (application) {
		menuitem = pixmap_menu_item_new (g_strconcat ("Open '", file_name_display, "' with ", application->name, NULL), NULL, 0);
		gtk_menu_append (GTK_MENU (document_popup), menuitem);

		gtk_object_set_data (GTK_OBJECT (menuitem), "full_path", (gpointer) file_path);

		gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (exec_document_wrap), (gpointer) NULL);

		gtk_widget_show (menuitem);

		applications = gnome_vfs_mime_get_short_list_applications ((char *)file_info->mime_type);
		
		if (applications) {
			GtkWidget *appmenu;
			GtkWidget *appmenuitem;
			
			menuitem = pixmap_menu_item_new ("Open With...", NULL, 0);
			gtk_menu_append (GTK_MENU (document_popup), menuitem);
			
			gtk_widget_show (menuitem);

			appmenu = scroll_menu_new ();
			gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), appmenu);

			gtk_widget_show (appmenu);

			while (applications) {
				appmenuitem = pixmap_menu_item_new (((GnomeVFSMimeApplication *) applications->data)->name, NULL, 0);

				gtk_object_set_data (GTK_OBJECT (appmenuitem), "full_path", (gpointer) file_path);
				gtk_signal_connect (GTK_OBJECT (appmenuitem), "activate",
						    GTK_SIGNAL_FUNC (exec_document_wrap), (gpointer) applications->data);
 
				gtk_widget_show (appmenuitem);

				gtk_menu_append (GTK_MENU (appmenu), appmenuitem);
				
				applications = applications->next;
			}
		}

	}

	menuitem = pixmap_menu_item_new (g_strconcat ("Copy '", file_name_display, "' to ...", NULL), NULL, 0);
	gtk_menu_append (GTK_MENU (document_popup), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (show_copy_dialog), 
			    (gpointer) file_path);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new (g_strconcat ("Move '", file_name_display, "' to ...", NULL), NULL, 0);
	gtk_menu_append (GTK_MENU (document_popup), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (show_move_dialog), 
			    (gpointer) file_path);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new (g_strconcat ("Rename '", file_name_display, "' to...", NULL), NULL, 0);
	gtk_menu_append (GTK_MENU (document_popup), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (show_rename_dialog), 
			    (gpointer) file_path);
	gtk_widget_show (menuitem);

	menuitem = pixmap_menu_item_new (g_strconcat ("Delete '", file_name_display, "'", NULL), NULL, 0);
	gtk_menu_append (GTK_MENU (document_popup), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (delete_file),
			    (gpointer) pmenu);
	gtk_widget_show (menuitem);

	if (application) {
		menuitem = pixmap_menu_item_new ("Change Handler Application..", NULL, 0);
	} else {
		menuitem = pixmap_menu_item_new ("Add Handler Application...", NULL, 0);
	}
	gtk_menu_append (GTK_MENU (document_popup), menuitem);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (show_handler),
			    (gpointer) file_info->mime_type);
	gtk_widget_show (menuitem);

	return document_popup;
}

