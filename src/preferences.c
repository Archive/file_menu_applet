/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <gnome.h>
#include <applet-widget.h>
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include "main.h"

/*
extern GHashTable *directories;
extern GtkWidget *documents_item;
extern gboolean icon_menus;
*/

void setup_entry_config (GConfClient* client, GtkWidget *widget, gchar *config_key);
void setup_bool_config (GConfClient* client, GtkWidget *widget, gchar *config_key);
void set_menu_title (GConfClient* client, 
		     guint cnxn_id, 
		     GConfEntry *entry, 
		     gpointer user_data);
void change_watched_directory (GConfClient* client,
			       guint cnxn_id,
			       GConfEntry *entry,
			       gpointer user_data);
void change_icon_setting (GConfClient* client,
			   guint cnxn_id,
			   GConfEntry *entry,
			   gpointer user_data);
void prefs_dialog_update_sensitivity2 (void);
void changed_data ();
/*void prefs_dialog_destroy_callback (GtkObject       *object,
  gpointer         user_data);*/
void on_directory_entry_changed (GtkEditable     *editable,
				 gpointer         user_data);
void on_directory_entry_destroy (GtkObject       *object,
				 gpointer         user_data);
void on_title_entry_changed (GtkEditable     *editable,
			     gpointer         user_data);
void on_title_entry_destroy (GtkObject       *object,
			     gpointer         user_data);
void on_icon_toggle_toggled (GtkToggleButton *togglebutton,
			      gpointer         user_data);
void on_icon_toggle_destroy (GtkObject       *object,
			      gpointer         user_data);
void changed_data ();
static GConfChangeSet* prefs_dialog_get_change_set (GtkWidget* dialog);
static void prefs_dialog_update_sensitivity(GtkWidget* dialog);
static void prefs_dialog_apply(GtkWidget* dialog);
static void apply_button_callback(GtkWidget* button, gpointer data);
static void ok_button_callback(GtkWidget* button, gpointer data);
static void cancel_button_callback(GtkWidget* button, gpointer data);
static void prefs_dialog_destroy_callback(GtkWidget* dialog, gpointer data);
static void prefs_dialog_destroyed(GtkWidget* dialog, gpointer main_window);
void show_preferences_menu (AppletWidget *widget, gpointer data);
static GtkWidget* create_prefs_dialog(GtkWidget* parent, GConfClient* client);

void
setup_entry_config (GConfClient* client, GtkWidget *widget, gchar *config_key)
{
	GConfValue* initial = NULL;
	
	initial = gconf_client_get(client, config_key, NULL);

#if 0
	if (initial != NULL && initial->type == GCONF_VALUE_STRING) {
		const gchar* str = gconf_value_get_string(initial);
		/*		gtk_entry_set_text(GTK_ENTRY(entry), widget);*/
	}
#endif

	if (initial)
		gconf_value_free(initial);
	
	gtk_object_set_data(GTK_OBJECT(widget), "client", client);
	gtk_object_set_data(GTK_OBJECT(widget), "key", g_strdup(config_key));
	
	/*	gtk_signal_connect(GTK_OBJECT(entry), "destroy",
			   config_entry_destroy_callback,
			   NULL);
	gtk_signal_connect(GTK_OBJECT(entry), "changed",
			   config_entry_changed_callback,
			   prefs_dialog);*/
	gtk_widget_set_sensitive (widget,
				  gconf_client_key_is_writable (client,
								config_key, NULL));
	

}

void
setup_bool_config (GConfClient* client, GtkWidget *widget, gchar *config_key)
{
	GConfValue* initial = NULL;
	
	initial = gconf_client_get(client, config_key, NULL);

	/*	if (initial != NULL && initial->type == GCONF_VALUE_BOOL) {
		const gchar* str = gconf_value_get_string(initial);
		gtk_entry_set_text(GTK_ENTRY(entry), widget);
	}
	*/
	if (initial)
		gconf_value_free(initial);
	
	gtk_object_set_data(GTK_OBJECT(widget), "client", client);
	gtk_object_set_data(GTK_OBJECT(widget), "key", g_strdup(config_key));
	/*	
	gtk_signal_connect(GTK_OBJECT(entry), "destroy",
			   config_entry_destroy_callback,
			   NULL);
	gtk_signal_connect(GTK_OBJECT(entry), "changed",
			   config_entry_changed_callback,
			   prefs_dialog);
	gtk_widget_set_sensitive (widget,
				  gconf_client_key_is_writable (client,
								config_key, NULL));
	*/

}

void 
set_menu_title (GConfClient* client,
		guint cnxn_id,
		GConfEntry *entry,
		gpointer user_data)
{
	GtkWidget *label;
	
	label = GTK_WIDGET (gtk_container_children (GTK_CONTAINER (user_data))->data);
	gtk_label_set_pattern (GTK_LABEL (label), NULL);
	gtk_label_set_text (GTK_LABEL (label), entry->value->d.string_data);

	return;
}


void 
change_watched_directory (GConfClient* client,
			  guint cnxn_id,
			  GConfEntry *entry,
			  gpointer user_data)
{
	/*	directory_menu *dm;*/
	guint handler;

	handler = (guint) gtk_object_get_data (GTK_OBJECT (fma->documents_item), "handler");
	gtk_signal_disconnect (GTK_OBJECT (fma->documents_item), handler);
	
	handler = gtk_signal_connect (GTK_OBJECT (fma->documents_item), 
					"activate", 
					GTK_SIGNAL_FUNC (show_directory_menu), 
					(gpointer) translate_directory_name (entry->value->d.string_data));
	gtk_object_set_data (GTK_OBJECT (fma->documents_item), "handler", (gpointer) handler);

	/*	dm = gtk_object_get_data (GTK_OBJECT (fma->documents_item), "dm");*/
	fma->dm->mtime = -1;
	fma->dm->name = translate_directory_name (entry->value->d.string_data);
	g_hash_table_insert (fma->directories, fma->documents_item, fma->dm);	
	
	/*	changed_data ();*/
	
}

void
change_icon_setting (GConfClient* client,
		      guint cnxn_id,
		      GConfEntry *entry,
		      gpointer user_data)
{
	fma->icon_menus = entry->value->d.bool_data;
	/*	changed_data ();*/
}

void
prefs_dialog_update_sensitivity2 (void)
{
	/*	if (gconf_change_set_size(changeset) > 0) {
		gtk_widget_set_sensitive(apply, TRUE);
	} else {
		gtk_widget_set_sensitive(apply, FALSE);
	}

	gtk_widget_set_sensitive (GTK_WIDGET (apply), FALSE);*/
}

/*void 
changed_data ()
{
	gtk_widget_set_sensitive (GTK_WIDGET (ok), TRUE);
	gtk_widget_set_sensitive (GTK_WIDGET (apply), TRUE);
	}*/

/*void
prefs_dialog_destroy_callback          (GtkObject       *object,
                                        gpointer         user_data)
{

}
*/

void
on_directory_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{
	gchar* text;
	GtkWidget* prefs_dialog = user_data;
	GConfChangeSet* cs;
	
	cs = prefs_dialog_get_change_set(prefs_dialog);
	text = gtk_editable_get_chars(GTK_EDITABLE(editable), 0, -1);
	gconf_change_set_set_string (cs, fma->gconf_key_dir, text);
	
	prefs_dialog_update_sensitivity(prefs_dialog);
}


void
on_directory_entry_destroy             (GtkObject       *object,
                                        gpointer         user_data)
{

}


void
on_title_entry_changed                 (GtkEditable     *editable,
                                        gpointer         user_data)
{
	gchar* text;
	GtkWidget* prefs_dialog = user_data;
	GConfChangeSet* cs;

	cs = prefs_dialog_get_change_set(prefs_dialog);
	text = gtk_editable_get_chars(GTK_EDITABLE(editable), 0, -1);
	gconf_change_set_set_string (cs, fma->gconf_key_menu, text);

	prefs_dialog_update_sensitivity(prefs_dialog);
}


void
on_title_entry_destroy                 (GtkObject       *object,
                                        gpointer         user_data)
{

}


void
on_icon_toggle_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget* prefs_dialog = user_data;
	GConfChangeSet* cs;

	cs = prefs_dialog_get_change_set(prefs_dialog);
	gconf_change_set_set_bool (cs, fma->gconf_key_icon, 
				   gtk_toggle_button_get_active (togglebutton));
	
	prefs_dialog_update_sensitivity(prefs_dialog);
}


void
on_icon_toggle_destroy                (GtkObject       *object,
                                        gpointer         user_data)
{

}

static GConfChangeSet*
prefs_dialog_get_change_set(GtkWidget* dialog)
{
  return gtk_object_get_data(GTK_OBJECT(dialog), "changeset");
}

static void
prefs_dialog_update_sensitivity(GtkWidget* dialog)
{
  GtkWidget* apply;
  GtkWidget* ok;
  GtkWidget* cancel;
  GConfChangeSet* cs;
  
  apply = gtk_object_get_data(GTK_OBJECT(dialog), "apply_button");
  ok = gtk_object_get_data(GTK_OBJECT(dialog), "ok_button");
  cancel = gtk_object_get_data(GTK_OBJECT(dialog), "cancel_button");

  g_assert(apply != NULL);
  
  cs = prefs_dialog_get_change_set(dialog);

  if (gconf_change_set_size(cs) > 0) {
	  gtk_widget_set_sensitive(apply, TRUE);
  } else {
	  gtk_widget_set_sensitive(apply, FALSE);
  }
}

static void
prefs_dialog_apply(GtkWidget* dialog)
{
  GConfChangeSet* cs;
  GConfClient* client;
  
  client = gtk_object_get_data(GTK_OBJECT(dialog), "client");

  cs = gtk_object_get_data(GTK_OBJECT(dialog), "changeset");
  
  /* again, relying on default error handler. The third argument here
     is whether to remove the successfully-committed items from the
     change set; here we remove the already-committed stuff, so if the
     user resolves the error, they can bang on "apply" some more and
     commit the remaining items. The return value indicates whether an
     error occurred. */
  gconf_client_commit_change_set(client, cs, TRUE, NULL);
  
  prefs_dialog_update_sensitivity(dialog);
}

static void
apply_button_callback(GtkWidget* button, gpointer data)
{
  prefs_dialog_apply(data);
}

static void
ok_button_callback(GtkWidget* button, gpointer data)
{
  GtkWidget* dialog = data;
  
  prefs_dialog_apply(dialog);
  gtk_widget_hide(dialog);
}

static void
cancel_button_callback(GtkWidget* button, gpointer data)
{
  GtkWidget* dialog = data;
  GConfChangeSet* cs;
  GConfClient* client;
  gchar *str;
  gboolean val;
  GtkWidget *directory_entry;
  GtkWidget *title_entry;
  GtkWidget *icon_toggle;

  client = gtk_object_get_data(GTK_OBJECT(data), "client");
  cs = gtk_object_get_data(GTK_OBJECT(dialog), "changeset");

  directory_entry = gtk_object_get_data (GTK_OBJECT (dialog), "directory_entry");
  str = gconf_client_get_string (client, fma->gconf_key_dir, NULL);
  str = NULL;
  str = str ? str : _("~/");
  gtk_entry_set_text (GTK_ENTRY (directory_entry), str);
  
  title_entry = gtk_object_get_data (GTK_OBJECT (dialog), "title_entry");
  str = NULL;
  str = gconf_client_get_string (client, fma->gconf_key_menu, NULL);
  str = str ? str : _("Home Directory");
  gtk_entry_set_text (GTK_ENTRY (title_entry), str);
  
  icon_toggle = gtk_object_get_data (GTK_OBJECT (dialog), "icon_toggle");
  val = gconf_client_get_bool (client, fma->gconf_key_icon, NULL);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (icon_toggle), val);
  
  gconf_change_set_clear(cs);

  gtk_widget_hide(dialog);
}

static void
prefs_dialog_destroy_callback(GtkWidget* dialog, gpointer data)
{
  GConfChangeSet* cs;
  GConfClient* client;

  client = gtk_object_get_data(GTK_OBJECT(dialog), "client");
  
  cs = gtk_object_get_data(GTK_OBJECT(dialog), "changeset");
  
  gconf_change_set_unref(cs);

  gtk_object_unref(GTK_OBJECT(client));
}

static void
prefs_dialog_destroyed(GtkWidget* dialog, gpointer main_window)
{
  gtk_object_set_data(GTK_OBJECT(main_window), "prefs", NULL);
}

void 
show_preferences_menu (AppletWidget *widget, gpointer data)
{
	GtkWidget* prefs_dialog;
	GtkWidget* main_window = data;
	GConfClient* client;
	
	prefs_dialog = gtk_object_get_data(GTK_OBJECT(main_window), "prefs");
	
	if (prefs_dialog == NULL) {
		client = gtk_object_get_data(GTK_OBJECT(main_window), "client");
		
		prefs_dialog = create_prefs_dialog(main_window, client);
		
		prefs_dialog_update_sensitivity (prefs_dialog);

		gtk_object_set_data(GTK_OBJECT(main_window), "prefs", prefs_dialog);
		
		gtk_signal_connect(GTK_OBJECT(prefs_dialog), "destroy",
				   GTK_SIGNAL_FUNC(prefs_dialog_destroyed),
				   main_window);
		
		gtk_widget_show_all(prefs_dialog);


	} else if (GTK_WIDGET_REALIZED(prefs_dialog)) {
		gtk_widget_show_all(prefs_dialog);
		gdk_window_raise(prefs_dialog->window);
	}
}


static GtkWidget*
create_prefs_dialog(GtkWidget* parent, GConfClient* client)
{
	GConfChangeSet *changeset = NULL;

	gchar *str;
	gboolean val;

	GtkWidget *preferences;
	GtkWidget *dialog_vbox1;
	GtkWidget *table1;
	GtkWidget *dir_label;
	GtkWidget *directory_combo;
	GList *directory_combo_items = NULL;
	GtkWidget *directory_entry;
	GtkWidget *menu_label;
	GtkWidget *title_entry;
	GtkWidget *speed_label;
	GtkWidget *icon_toggle;
	GtkWidget *dialog_action_area1;
	GtkWidget *hbox1;
	GtkWidget *ok_button;
	GtkWidget *apply_button;
	GtkWidget *cancel_button;

	preferences = gtk_dialog_new ();
	gtk_object_set_data (GTK_OBJECT (preferences), "preferences", preferences);
	gtk_window_set_title (GTK_WINDOW (preferences), _("File Menu Applet Preferences"));
	GTK_WINDOW (preferences)->type = GTK_WINDOW_DIALOG;
	gtk_window_set_policy (GTK_WINDOW (preferences), TRUE, TRUE, TRUE);
	gtk_window_set_wmclass (GTK_WINDOW (preferences), "prefs", "prefs");

	changeset = gconf_change_set_new ();
	gtk_object_set_data(GTK_OBJECT(preferences), "changeset", changeset);

	gtk_object_set_data(GTK_OBJECT(preferences), "client", client);
  
	dialog_vbox1 = GTK_DIALOG (preferences)->vbox;
	gtk_object_set_data (GTK_OBJECT (preferences), "dialog_vbox1", dialog_vbox1);
	gtk_widget_show (dialog_vbox1);

	table1 = gtk_table_new (3, 2, FALSE);
	gtk_widget_ref (table1);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "table1", table1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (table1);
	gtk_box_pack_start (GTK_BOX (dialog_vbox1), table1, TRUE, TRUE, 10);
	gtk_container_set_border_width (GTK_CONTAINER (table1), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table1), 15);
	gtk_table_set_col_spacings (GTK_TABLE (table1), 15);

	dir_label = gtk_label_new (_("Directory to watch"));
	gtk_widget_ref (dir_label);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "dir_label", dir_label,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (dir_label);
	gtk_table_attach (GTK_TABLE (table1), dir_label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (dir_label), 0, 0.5);

	directory_combo = gtk_combo_new ();
	gtk_widget_ref (directory_combo);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "directory_combo", directory_combo,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (directory_combo);
	gtk_table_attach (GTK_TABLE (table1), directory_combo, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_combo_set_case_sensitive (GTK_COMBO (directory_combo), TRUE);
	
	directory_combo_items = g_list_append (directory_combo_items, (gpointer) _("~/"));
	directory_combo_items = g_list_append (directory_combo_items, (gpointer) _("~/doc"));
	directory_combo_items = g_list_append (directory_combo_items, (gpointer) _("/tmp"));
	directory_combo_items = g_list_append (directory_combo_items, (gpointer) _("/"));
	directory_combo_items = g_list_append (directory_combo_items, (gpointer) _("/etc"));
	gtk_combo_set_popdown_strings (GTK_COMBO (directory_combo), directory_combo_items);
	g_list_free (directory_combo_items);

	directory_entry = GTK_COMBO (directory_combo)->entry;
	gtk_widget_ref (directory_entry);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "directory_entry", directory_entry,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (directory_entry);
	str = gconf_client_get_string (client, fma->gconf_key_dir, NULL);
	/*	str = NULL;*/
	str = str ? str : _("~/");
	gtk_entry_set_text (GTK_ENTRY (directory_entry), str);

	menu_label = gtk_label_new (_("Menu Title"));
	gtk_widget_ref (menu_label);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "menu_label", menu_label,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (menu_label);
	gtk_table_attach (GTK_TABLE (table1), menu_label, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (menu_label), 0, 0.5);

	title_entry = gtk_entry_new ();
	gtk_widget_ref (title_entry);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "title_entry", title_entry,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (title_entry);
	gtk_table_attach (GTK_TABLE (table1), title_entry, 1, 2, 1, 2,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	str = NULL;
	str = gconf_client_get_string (client, fma->gconf_key_menu, NULL);
	str = str ? str : _("Documents");
	gtk_entry_set_text (GTK_ENTRY (title_entry), str);

	speed_label = gtk_label_new (_("Speed"));
	gtk_widget_ref (speed_label);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "speed_label", speed_label,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (speed_label);
	gtk_table_attach (GTK_TABLE (table1), speed_label, 0, 1, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (speed_label), 0, 0.5);

	icon_toggle = gtk_check_button_new_with_label (_("Don't look up icons"));
	gtk_widget_ref (icon_toggle);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "icon_toggle", icon_toggle,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (icon_toggle);
	gtk_table_attach (GTK_TABLE (table1), icon_toggle, 1, 2, 2, 3,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	val = gconf_client_get_bool (client, fma->gconf_key_icon, NULL);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (icon_toggle), val);

	dialog_action_area1 = GTK_DIALOG (preferences)->action_area;
	gtk_object_set_data (GTK_OBJECT (preferences), "dialog_action_area1", dialog_action_area1);
	gtk_widget_show (dialog_action_area1);
	gtk_container_set_border_width (GTK_CONTAINER (dialog_action_area1), 10);

	hbox1 = gtk_hbox_new (TRUE, 0);
	gtk_widget_ref (hbox1);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "hbox1", hbox1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (dialog_action_area1), hbox1, TRUE, TRUE, 0);

	ok_button = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_widget_ref (ok_button);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "ok_button", ok_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (ok_button);
	gtk_box_pack_start (GTK_BOX (hbox1), ok_button, FALSE, FALSE, 0);

	apply_button = gnome_stock_button (GNOME_STOCK_BUTTON_APPLY);
	gtk_widget_ref (apply_button);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "apply_button", apply_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (apply_button);
	gtk_box_pack_start (GTK_BOX (hbox1), apply_button, FALSE, FALSE, 0);

	cancel_button = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	gtk_widget_ref (cancel_button);
	gtk_object_set_data_full (GTK_OBJECT (preferences), "cancel_button", cancel_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (cancel_button);
	gtk_box_pack_start (GTK_BOX (hbox1), cancel_button, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT (preferences), "destroy",
			    GTK_SIGNAL_FUNC (prefs_dialog_destroy_callback),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (directory_entry), "changed",
			    GTK_SIGNAL_FUNC (on_directory_entry_changed),
			    preferences);
	gtk_signal_connect (GTK_OBJECT (directory_entry), "destroy",
			    GTK_SIGNAL_FUNC (on_directory_entry_destroy),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (title_entry), "changed",
			    GTK_SIGNAL_FUNC (on_title_entry_changed),
			    preferences);
	gtk_signal_connect (GTK_OBJECT (title_entry), "destroy",
			    GTK_SIGNAL_FUNC (on_title_entry_destroy),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (icon_toggle), "toggled",
			    GTK_SIGNAL_FUNC (on_icon_toggle_toggled),
			    preferences);
	gtk_signal_connect (GTK_OBJECT (icon_toggle), "destroy",
			    GTK_SIGNAL_FUNC (on_icon_toggle_destroy),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (ok_button), "clicked",
			    GTK_SIGNAL_FUNC (ok_button_callback),
			    preferences);
	gtk_signal_connect (GTK_OBJECT (apply_button), "clicked",
			    GTK_SIGNAL_FUNC (apply_button_callback),
			    preferences);
	gtk_signal_connect (GTK_OBJECT (cancel_button), "clicked",
			    GTK_SIGNAL_FUNC (cancel_button_callback),
			    preferences);

	return preferences;
}

