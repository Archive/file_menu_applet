/*
 * flist-mime-icon.h: utility functions for getting icons for mime
 * types
 *
 * Authors:
 *    Jacob Berkman   <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef _FLIST_MIME_ICON_H_
#define _FLIST_MIME_ICON_H_

#include <libgnomevfs/gnome-vfs-types.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define ICON_SIZE 18.0

char       *flist_mime_icon_get_file   (const char       *file_path,
					GnomeVFSFileType  file_type,
					const char       *mime_type);
GdkPixbuf  *flist_mime_icon_load       (const char       *file_path,
					GnomeVFSFileType  file_type,
					const char       *mime_type);
gboolean    flist_nautilus_theme_load  (void);
const char *flist_mime_get_icon        (const char       *mime_type);

const char *flist_get_thumbnail_image  (const char *path);
#endif /* _FLIST_MIME_ICON_H_ */
