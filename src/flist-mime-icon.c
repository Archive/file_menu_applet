/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/*
 * flist-mime-icon.c: utility functions for getting icons for mime
 * types
 *
 * Authors:
 *    Jacob Berkman   <jacob@ximian.com>
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>

#include "flist-mime-icon.h"

#include <libgnome/libgnome.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#define ICON_NAME_BLOCK_DEVICE          "i-blockdev.png"
#define ICON_NAME_BROKEN_SYMBOLIC_LINK  "i-symlink.png"
#define ICON_NAME_CHARACTER_DEVICE      "i-chardev.png"
#define ICON_NAME_DIRECTORY             "i-directory.png"
#define ICON_NAME_EXECUTABLE            "i-executable.png"
#define ICON_NAME_FIFO                  "i-fifo.png"
#define ICON_NAME_INSTALL		"services-rpm.png"
#define ICON_NAME_REGULAR               "i-regular.png"
#define ICON_NAME_REGULAR2               "i-regular-24.png"
#define ICON_NAME_SEARCH_RESULTS        "i-search.png"
#define ICON_NAME_SOCKET                "i-sock.png"
#define ICON_NAME_THUMBNAIL_LOADING     "loading.png"
#define ICON_NAME_TRASH_EMPTY           "trash-empty.png"
#define ICON_NAME_TRASH_NOT_EMPTY       "trash-full.png"
#define ICON_NAME_WEB			"i-web.png"

#define ICON_NAME_DESKTOP               "my_desktop.png"
#define ICON_NAME_HOME                  "my_home.png"
#define ICON_NAME_ROOT                  "my_computer.png"
#define ICON_NAME_DOCUMENTS             "my_documents.png"

#define N_ICON_DIR "/nautilus/"

static GHashTable *icon_hash = NULL;
static GHashTable *dir_hash = NULL;

gchar *nautilus_theme = NULL;
static gboolean use_nautilus_theme = TRUE;

static gchar *nautilus_theme_dir = NULL;

static void
create_hashes (void)
{
	g_return_if_fail (icon_hash == NULL);
	g_return_if_fail (dir_hash == NULL);

	icon_hash = g_hash_table_new (g_str_hash, g_str_equal);
	dir_hash  = g_hash_table_new (g_str_hash, g_str_equal);

	/* FIXME: don't leak */
	/* (note: only one leaks) */
	g_hash_table_insert (dir_hash, g_get_home_dir (), 
			     ICONDIR"/"ICON_NAME_HOME);
	g_hash_table_insert (dir_hash, gnome_util_prepend_user_home (".gnome-desktop"),
			     ICONDIR"/"ICON_NAME_DESKTOP);
	g_hash_table_insert (dir_hash, gnome_util_prepend_user_home ("Documents"),
			     ICONDIR"/"ICON_NAME_DOCUMENTS);
	g_hash_table_insert (dir_hash, "/", ICONDIR"/"ICON_NAME_ROOT);
}

const char *
flist_get_thumbnail_image (const char *path)
{
	char *ext;
	char *dir;
	char *filename;
	char *thumbnail;
	char *home;
	char *gvfs_path;

	/* So the thumbnails might be in $path/.thumbnails/$filename.png 
	   or $path/.thumbnails/$filename */

	// Find the last period.  This is the extension
	ext = strrchr (path, '.');
	filename = strrchr (path, '/') + 1;
	dir = g_strndup (path, filename - path);
	
	gvfs_path = gnome_vfs_get_uri_from_local_path (dir);
	gvfs_path[strlen(gvfs_path)-1] = '\0';

	home = g_strconcat (g_get_home_dir(), 
			    "/.nautilus/thumbnails/", 
			    gnome_vfs_escape_slashes (gvfs_path),
			    NULL);

	g_free (gvfs_path);

	if (ext && 0 == strcmp (ext, ".png")) {
		thumbnail = g_strconcat (home, "/", filename, NULL);
		if (g_file_exists (thumbnail)) {
			g_free (dir);
			return thumbnail;
		}

		g_free (thumbnail);

		thumbnail = g_strconcat (dir, ".thumbnails/", filename, NULL);
		g_free (dir);
		if (g_file_exists (thumbnail)) {
			return thumbnail;
		}
		g_free (thumbnail);
		return NULL;
	}
	
	thumbnail = g_strconcat (home, "/", filename, ".png", NULL);
	if (g_file_exists (thumbnail)) {
		g_free (dir);
		return thumbnail;
	}

	g_free (thumbnail);

	thumbnail = g_strconcat (dir, ".thumbnails/", filename, ".png", NULL);
	g_free (dir);
	if (g_file_exists (thumbnail)) {
		return thumbnail;
	}
	g_free (thumbnail);
	return NULL;
}

gboolean
flist_nautilus_theme_load (void)
{
	gchar *gnome_path = getenv ("GNOME_PATH");
	gchar **gnome_paths;
	
	if (use_nautilus_theme == TRUE) {
		if (nautilus_theme == NULL) {
			use_nautilus_theme = FALSE;
			return FALSE;
		}
		
		if (nautilus_theme_dir != NULL)
			return TRUE;
		
		
		nautilus_theme_dir = g_strconcat (g_get_home_dir (), 
						  "/.nautilus/themes/", 
						  nautilus_theme, NULL);
		
		if (g_file_exists (nautilus_theme_dir))
			return TRUE;
		
		g_free (nautilus_theme_dir);
		nautilus_theme_dir = NULL;
		
#ifdef GNOME_DATADIR
		nautilus_theme_dir = g_strconcat (GNOME_DATADIR, 
						  "/pixmaps/nautilus/", 
						  nautilus_theme, NULL);
		if (g_file_exists (nautilus_theme_dir))
			return TRUE;
#endif
		
		if (gnome_path) {
			int length = 0;
			gnome_paths = g_strsplit (gnome_path, ":", 0);
			
			while (gnome_paths[length])
				length ++;
			
			for (length --; length >= 0; length --) {
				nautilus_theme_dir = g_strconcat (gnome_paths[length], 
								  "/share/pixmaps/nautilus/", 
								  nautilus_theme, NULL);
				if (g_file_exists (nautilus_theme_dir)) {
					g_strfreev (gnome_paths);
					return TRUE;
				}
				g_free (nautilus_theme_dir);
			}
			
			g_strfreev (gnome_paths);
			
			nautilus_theme_dir = g_strconcat (MIMEICONDIR, 
							  N_ICON_DIR, 
							  nautilus_theme, NULL);
			
			if (!g_file_exists (nautilus_theme_dir)) {
				g_free (nautilus_theme_dir);
				nautilus_theme_dir = NULL;
				use_nautilus_theme = FALSE;
				return FALSE;
			}
		}
	}
	return TRUE;
}

const char *
flist_mime_get_icon (const char *mime_type)
{
	gchar *mime_part, *index, *icon_name, *icon_name2;
	
	if (!mime_type)
		goto skip_theme_stuff;
	
	if (flist_nautilus_theme_load ()) {
		
		// Find the icon!
		if (!strcmp (mime_type, "text/plain")) {
			icon_name = g_strconcat (nautilus_theme_dir, 
						 "/", 
						 ICON_NAME_REGULAR, NULL);
			icon_name2 = g_strconcat (nautilus_theme_dir, 
						  "/", 
						  ICON_NAME_REGULAR2, NULL);
		} else {
			
			mime_part = g_strdup (mime_type);
			index = strchr (mime_part, '/');
			*index = '-';
			
			icon_name = g_strconcat (nautilus_theme_dir, 
						 "/gnome-", mime_part, ".png", NULL);
			icon_name2 = g_strconcat (nautilus_theme_dir, 
						  "/gnome-", mime_part, "-24.png", NULL);
			
			g_free (mime_part);
		}
		
		if (g_file_exists (icon_name2)) {
			g_free (icon_name);
			return icon_name2;
		}
		
		g_free (icon_name2);
		
		if (g_file_exists (icon_name))
			return icon_name;
		
		g_free (icon_name);
	}
	
 skip_theme_stuff:
	
	return gnome_vfs_mime_get_icon (mime_type);
}

char *
flist_mime_icon_get_file (const char *file_path, GnomeVFSFileType file_type, const char *mime_type)
{
	const char *icon = NULL;
	char *file = NULL;

	if (!dir_hash)
		create_hashes ();

	switch (file_type) {
	case GNOME_VFS_FILE_TYPE_SYMBOLIC_LINK:
		icon = ICON_NAME_BROKEN_SYMBOLIC_LINK;
	default:
	case GNOME_VFS_FILE_TYPE_UNKNOWN:
	case GNOME_VFS_FILE_TYPE_REGULAR:
	        if (file_path) {
 		        icon = flist_get_thumbnail_image (file_path);
			if (icon) break;
		}
		if (mime_type) 
			icon = flist_mime_get_icon (mime_type);
		if (!icon) 
			icon = ICON_NAME_REGULAR;			
		break;
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		file = g_hash_table_lookup (
			dir_hash,
			strstr (file_path, "file:///") == file_path
			? file_path + 7 
			: file_path);
		icon = ICON_NAME_DIRECTORY;
		break;
	case GNOME_VFS_FILE_TYPE_CHARACTER_DEVICE:
		icon = ICON_NAME_CHARACTER_DEVICE;
		break;
	case GNOME_VFS_FILE_TYPE_FIFO:
		icon = ICON_NAME_FIFO;
		break;
	case GNOME_VFS_FILE_TYPE_SOCKET:
		icon = ICON_NAME_SOCKET;
		break;
	case GNOME_VFS_FILE_TYPE_BLOCK_DEVICE:
		icon = ICON_NAME_BLOCK_DEVICE;
		break;
	}

	if (!file) {
	  if (!icon) return NULL;
	  flist_nautilus_theme_load ();

	  if (!g_file_exists (icon)) {
	    if (nautilus_theme_dir) file = g_concat_dir_and_file (nautilus_theme_dir, icon);

	    if (!file || !g_file_exists (file)) {
	      g_free (file);
	      
	      file = g_concat_dir_and_file (MIMEICONDIR, icon);
	      if (!g_file_exists (file)) {
		g_free (file);
		
		file = g_strconcat (MIMEICONDIR, "/nautilus/", icon, NULL);
		if (!g_file_exists (file)) {
		  g_free (file);
		
		  return NULL;
		}
	      }
	    }
	  }
	  else
	    file = g_strdup (icon);
	} else {
	  /* g_print ("got magic icon: %s\n", file); */
		file = g_strdup (file);
	}

	return file;
}

GdkPixbuf *
flist_mime_icon_load (const char *file_path, GnomeVFSFileType file_type, const char *mime_type)
{
	GdkPixbuf *pb, *pb2;
	char *file = NULL;

	file = flist_mime_icon_get_file (file_path, file_type, mime_type);

	if (!file)
		return NULL;

	if (!icon_hash)
		create_hashes ();

	if (g_hash_table_lookup_extended (icon_hash, file, NULL, (gpointer *) &pb)) {
		g_free (file);
  	        if (pb)
		         gdk_pixbuf_ref (pb);
		return pb;
	}

	pb2 = gdk_pixbuf_new_from_file (file);
	if (pb2) {
		double ar = (double)gdk_pixbuf_get_width (pb2) / (double)gdk_pixbuf_get_height (pb2);
		/* g_print ("ar: %f, width: %d\n", ar, (int)(ICON_SIZE * ar)); */
		pb = gdk_pixbuf_scale_simple (pb2, ar < 1 ? ICON_SIZE * ar : ICON_SIZE, ar > 1 ? ICON_SIZE / ar : ICON_SIZE, GDK_INTERP_HYPER);
		gdk_pixbuf_unref (pb2);
	} else {
		g_message ("Could not load: `%s', trying default", file);
		g_free (file);
		g_hash_table_insert (icon_hash, file, NULL);
		pb = g_hash_table_lookup (icon_hash, MIMEICONDIR"/"ICON_NAME_REGULAR);
		if (pb) gdk_pixbuf_ref (pb);
		return pb;
	}
	/* may as well... */
	gdk_pixbuf_ref (pb);
	g_hash_table_insert (icon_hash, file, pb);
	return pb;
}

