#include <config.h>
#include "title-item.h"

static void 
title_item_class_init (TitleItemClass * c)
{
	GtkItemClass *gtk_item_class = (GtkItemClass *)c;
	gtk_item_class->select = NULL;
}

GtkWidget * 
title_item_new (void)
{
	return GTK_WIDGET(gtk_type_new(title_item_get_type()));
}

guint
title_item_get_type (void)
{
	static guint type = 0;

	if ( ! type) {
		static const GtkTypeInfo info = {
			"TitleItem",
			sizeof (TitleItem),
			sizeof (TitleItemClass),
			(GtkClassInitFunc) title_item_class_init,
			(GtkObjectInitFunc) NULL,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL
		};

		type = gtk_type_unique (PARENT_TYPE, &info);
	}

	return type;
}
