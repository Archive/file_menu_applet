/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * q-multi-menu.c
 * Copyright 2002, Chris Lahey.
 *
 * Authors:
 *   Chris Lahey <clahey@ximian.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License, version 2, as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <config.h>

#include "q-multi-menu.h"

#include <gtk/gtksignal.h>
#include <gtk/gtksignal.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define MENU_CLASS(e) ((QMultiMenuClass *)((GtkObject *)e)->klass)

#define PARENT_TYPE (gtk_object_get_type ())
#define d(x)

static GtkObjectClass *parent_class;

enum {
	CHANGED,
	LIST_CHANGED,
	LIST_INSERTED,
	LIST_DELETED,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0, };

typedef struct {
	char *string;
	gpointer datum;
} QMultiMenuItem;

typedef struct {
	int count;
	QMultiMenuItem *items;
	char *label;
} QMultiMenuList;

struct QMultiMenuPrivate_ {
	int count;
	QMultiMenuList **lists;

	int item_count;
	QMultiMenuItem *items;

	guint idle_id;
	int prefix_length;
	char *prefix;

	int maximum;
	int frozen_count;

	guint changed : 1;
};

static void
q_multi_menu_changed (QMultiMenu *menu)
{
	gtk_signal_emit (GTK_OBJECT (menu),
			 signals [CHANGED]);
}

#if 0
static void
q_multi_menu_list_changed (QMultiMenu *menu, int list_idx)
{
	gtk_signal_emit (GTK_OBJECT (menu),
			 signals [LIST_CHANGED],
			 list_idx);
}

static void
q_multi_menu_list_inserted (QMultiMenu *menu, int list_idx, int pos, int count)
{
	gtk_signal_emit (GTK_OBJECT (menu),
			 signals [LIST_INSERTED],
			 list_idx, pos, count);
}

static void
q_multi_menu_list_deleted (QMultiMenu *menu, int list_idx, int pos, int count)
{
	gtk_signal_emit (GTK_OBJECT (menu),
			 signals [LIST_DELETED],
			 list_idx, pos, count);
}
#endif

static int
compar (const void *a, const void *b)
{
	const QMultiMenuItem *ac = a;
	const QMultiMenuItem *bc = b;

	return strcasecmp (ac->string, bc->string);
}

static gboolean
rebuild (gpointer data)
{
	QMultiMenu *menu = data;
	int counts[27];
	int i;
	int total_count;

	menu->priv->idle_id = 0;
	menu->priv->changed = FALSE;

	qsort (menu->priv->items, menu->priv->item_count, sizeof (QMultiMenuItem), compar);

	total_count = 0;
	for (i = 0; i < 27; i++)
		counts[i] = 0;

	for (i = 0; i < menu->priv->item_count; i++) {
		char start_byte = *(menu->priv->items[i].string + menu->priv->prefix_length);

		if (start_byte == 0)
			continue;

		start_byte = tolower (start_byte);

		if (start_byte >= 'a' &&
		    start_byte <= 'z')
			counts[start_byte - 'a' + 1] ++;
		else
			counts[0]++;
		total_count ++;
	}

	for (i = 0; i < menu->priv->count; i++) {
		g_free (menu->priv->lists[i]->label);
		g_free (menu->priv->lists[i]);
	}
	g_free (menu->priv->lists);

	if (total_count <= menu->priv->maximum) {
		int j;
		menu->priv->count = 1;
		menu->priv->lists = g_new (QMultiMenuList *, 1);
		menu->priv->lists[0] = g_new (QMultiMenuList, 1);
		menu->priv->lists[0]->label = NULL;
		menu->priv->lists[0]->count = total_count;
		menu->priv->lists[0]->items = g_new (QMultiMenuItem, total_count);
		j = 0;
		for (i = 0; i < menu->priv->item_count; i++) {
			if (*(menu->priv->items[i].string + menu->priv->prefix_length) != 0)
				menu->priv->lists[0]->items[j++] = menu->priv->items[i];
		}
	} else {
		int this_count = 0;
		int j;
		char start[27];
		char end[27];
		int pos[27];
		int count[27];
		int list_count;

		pos[0] = 0;
		count[0] = counts[0];

		j = 1;
		start[j] = 'a';
		count[j] = 0;
		pos[j] = 0;

		for (i = 1; i < 27; i++) {
			if (counts[i] && this_count && this_count + counts[i] > menu->priv->maximum) {
				end[j] = ('a' - 1 ) + i - 1;

				j ++;

				start[j] = ('a' - 1) + i;
				count[j] = 0;
				pos[j] = 0;

				this_count = 0;
			}

			this_count += counts[i];
			count[j] += counts[i];
		}
		end[j] = 'z';
		j ++;

		list_count = j;

		menu->priv->count = list_count;
		menu->priv->lists = g_new (QMultiMenuList *, list_count);
		menu->priv->lists[0] = g_new (QMultiMenuList, 1);
		menu->priv->lists[0]->label = g_strdup ("123");
		menu->priv->lists[0]->count = count[0];
		menu->priv->lists[0]->items = g_new (QMultiMenuItem, count[0]);

		for (j = 0; j < count[0]; j++) {
			menu->priv->lists[0]->items[j].string = NULL;
			menu->priv->lists[0]->items[j].datum = NULL;
		}

		for (i = 1; i < list_count; i++) {
			menu->priv->lists[i] = g_new (QMultiMenuList, 1);
			menu->priv->lists[i]->count = count[i];
			menu->priv->lists[i]->items = g_new (QMultiMenuItem, count[i]);
			if (start[i] == end[i])
				menu->priv->lists[i]->label = g_strdup_printf ("%c", start[i]);
			else
				menu->priv->lists[i]->label = g_strdup_printf ("%c - %c", start[i], end[i]);

			for (j = 0; j < count[i]; j++) {
				menu->priv->lists[i]->items[j].string = NULL;
				menu->priv->lists[i]->items[j].datum = NULL;
			}
		}

		j = 1;

		for (i = 0; i < menu->priv->item_count; i++) {
			char start_byte = *(menu->priv->items[i].string + menu->priv->prefix_length);
			if (start_byte == 0)
				continue;

			start_byte = tolower (start_byte);

			if (start_byte >= 'a' &&
			    start_byte <= 'z') {
				if (start_byte > end[j])
					j++;
				menu->priv->lists[j]->items[pos[j]++] = menu->priv->items[i];
			} else {
				menu->priv->lists[0]->items[pos[0]++] = menu->priv->items[i];
			}
		}
	}

	q_multi_menu_changed (menu);

	return FALSE;
}

static void
changed (QMultiMenu *menu)
{
	menu->priv->changed = TRUE;
	if (menu->priv->idle_id == 0 && menu->priv->frozen_count <= 0) {
		menu->priv->idle_id =
			g_idle_add_full (G_PRIORITY_LOW, rebuild, menu, NULL);
	}
}

static void
maybe_rebuild (QMultiMenu *menu)
{
	if (menu->priv->changed) {
		g_source_remove (menu->priv->idle_id);
		menu->priv->idle_id = 0;
		rebuild (menu);
	}
}

typedef void (*GtkSignal_NONE__INT_INT_INT) (GtkObject * object,
					     gint arg1,
					     gint arg2,
					     gint arg3,
					     gpointer user_data);

static void
q_marshal_NONE__INT_INT_INT (GtkObject * object,
			     GtkSignalFunc func,
			     gpointer func_data, GtkArg * args)
{
	GtkSignal_NONE__INT_INT_INT rfunc;
	rfunc = (GtkSignal_NONE__INT_INT_INT) func;
	rfunc (object,
	       GTK_VALUE_INT (args[0]),
	       GTK_VALUE_INT (args[1]),
	       GTK_VALUE_INT (args[2]),
	       func_data);
}


static void
q_multi_menu_destroy (GtkObject *object)
{
	int i;
	QMultiMenu *menu = Q_MULTI_MENU (object);

	if (menu->priv == NULL)
		return;

	for (i = 0; i < menu->priv->count; i++) {
		g_free (menu->priv->lists[i]->label);
		g_free (menu->priv->lists[i]);
	}

	for (i = 0; i < menu->priv->lists[i]->count; i++)
		g_free (menu->priv->items[i].string);

	g_free (menu->priv->items);
	g_free (menu->priv->lists);
	g_free (menu->priv->prefix);
	g_free (menu->priv);

	menu->priv = NULL;

	if (parent_class->destroy)
		(*parent_class->destroy)(object);
}

static void
q_multi_menu_class_init (GtkObjectClass *object_class)
{
	QMultiMenuClass *klass = Q_MULTI_MENU_CLASS(object_class);
	parent_class = gtk_type_class (PARENT_TYPE);
	
	signals [CHANGED] =
		gtk_signal_new ("changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QMultiMenuClass, changed),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	signals [LIST_CHANGED] =
		gtk_signal_new ("list_changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QMultiMenuClass, list_changed),
				gtk_marshal_NONE__INT,
				GTK_TYPE_NONE, 1, GTK_TYPE_INT);

	signals [LIST_INSERTED] =
		gtk_signal_new ("list_inserted",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QMultiMenuClass, list_inserted),
				q_marshal_NONE__INT_INT_INT,
				GTK_TYPE_NONE, 3, GTK_TYPE_INT, GTK_TYPE_INT, GTK_TYPE_INT);

	signals [LIST_DELETED] =
		gtk_signal_new ("list_deleted",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QMultiMenuClass, list_deleted),
				q_marshal_NONE__INT_INT_INT,
				GTK_TYPE_NONE, 3, GTK_TYPE_INT, GTK_TYPE_INT, GTK_TYPE_INT);

	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	object_class->destroy = q_multi_menu_destroy;

	klass->changed        = NULL;
	klass->list_changed   = NULL;
	klass->list_inserted  = NULL;
	klass->list_deleted   = NULL;
}

#define MAXIMUM 50

static void
q_multi_menu_init (QMultiMenu *menu)
{
	menu->priv = g_new (QMultiMenuPrivate, 1);
	menu->priv->count = 0;
	menu->priv->lists = NULL;
	menu->priv->items = NULL;
	menu->priv->item_count = 0;
	menu->priv->prefix = g_strdup ("");
	menu->priv->prefix_length = 0;
	menu->priv->maximum = MAXIMUM;
	menu->priv->frozen_count = 0;
	menu->priv->changed = FALSE;
	menu->priv->idle_id = 0;
}

guint
q_multi_menu_get_type (void)
{
	static guint type = 0;
	
	if (!type)
	{
		GtkTypeInfo info =
		{
			"QMultiMenu",
			sizeof (QMultiMenu),
			sizeof (QMultiMenuClass),
			(GtkClassInitFunc) q_multi_menu_class_init,
			(GtkObjectInitFunc) q_multi_menu_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};
		
		type = gtk_type_unique (PARENT_TYPE, &info);
	}

  return type;
}

QMultiMenu *
q_multi_menu_new (void)
{
	return Q_MULTI_MENU (gtk_type_new (Q_MULTI_MENU_TYPE));
}

/**
 * q_multi_menu_count:
 * @menu: The q-multi-menu to operate on
 *
 * Returns: the number of columns in the multi menu.
 */
int
q_multi_menu_count (QMultiMenu *menu)
{
	g_return_val_if_fail (menu != NULL, 0);
	g_return_val_if_fail (Q_IS_MULTI_MENU (menu), 0);

	if (menu->priv == NULL)
		return 0;

	maybe_rebuild (menu);

	return menu->priv->count;
}


/**
 * q_multi_menu_list_label:
 * @menu: the e-multi-menu to operate on
 * @list_idx: The index of the list to access.
 *
 * Returns: the label of the @list_idx th list.
 */
const char *
q_multi_menu_list_label (QMultiMenu *menu,
			 int list_idx)
{
	g_return_val_if_fail (menu != NULL, NULL);
	g_return_val_if_fail (Q_IS_MULTI_MENU (menu), NULL);

	if (menu->priv == NULL)
		return NULL;

	maybe_rebuild (menu);

	g_return_val_if_fail (list_idx < menu->priv->count, NULL);
	g_return_val_if_fail (list_idx >= 0, NULL);

	return menu->priv->lists[list_idx]->label;
}

/**
 * q_multi_menu_list_length
 * @menu: the e-multi-menu to operate on
 * @list_idx: The index of the list to access.
 *
 * Returns: the length of the @list_idx th list.
 */
int
q_multi_menu_list_length (QMultiMenu *menu,
			  int list_idx)
{
	g_return_val_if_fail (menu != NULL, 0);
	g_return_val_if_fail (Q_IS_MULTI_MENU (menu), 0);

	if (menu->priv == NULL)
		return 0;

	maybe_rebuild (menu);

	g_return_val_if_fail (list_idx < menu->priv->count, 0);
	g_return_val_if_fail (list_idx >= 0, 0);

	return menu->priv->lists[list_idx]->count;
}

/**
 * q_multi_menu_list_nth:
 * @menu: the e-multi-menu to operate on
 * @list_idx: The index of the list to access.
 * @n: The list item to grab.
 *
 * Returns: the label of the @n th item of the @list_idx th list.
 */
const char *
q_multi_menu_list_nth (QMultiMenu *menu,
		       int list_idx,
		       int n)
{
	g_return_val_if_fail (menu != NULL, 0);
	g_return_val_if_fail (Q_IS_MULTI_MENU (menu), 0);

	if (menu->priv == NULL)
		return 0;

	maybe_rebuild (menu);

	g_return_val_if_fail (list_idx < menu->priv->count, NULL);
	g_return_val_if_fail (list_idx >= 0, NULL);

	g_return_val_if_fail (n < menu->priv->lists[list_idx]->count, NULL);
	g_return_val_if_fail (n >= 0, NULL);

	return menu->priv->lists[list_idx]->items[n].string;
}

/**
 * q_multi_menu_list_nth:
 * @menu: the e-multi-menu to operate on
 * @list_idx: The index of the list to access.
 * @n: The list item to grab.
 *
 * Returns: the label of the @n th item of the @list_idx th list.
 */
gpointer
q_multi_menu_list_nth_datum (QMultiMenu *menu,
			     int list_idx,
			     int n)
{
	g_return_val_if_fail (menu != NULL, 0);
	g_return_val_if_fail (Q_IS_MULTI_MENU (menu), 0);

	if (menu->priv == NULL)
		return 0;

	maybe_rebuild (menu);

	g_return_val_if_fail (list_idx < menu->priv->count, NULL);
	g_return_val_if_fail (list_idx >= 0, NULL);

	g_return_val_if_fail (n < menu->priv->lists[list_idx]->count, NULL);
	g_return_val_if_fail (n >= 0, NULL);

	return menu->priv->lists[list_idx]->items[n].datum;
}

/**
 * q_multi_menu_append:
 * @menu: The %EMultiMenu to operate on.
 * @item: The item to append.
 * 
 * Appends the given item to the q_multi_menu.
 **/
void
q_multi_menu_append (QMultiMenu *menu,
		     int count,
		     char const*const*item)
{
	q_multi_menu_append_with_data (menu, count, item, NULL);
}

/**
 * q_multi_menu_append:
 * @menu: The %EMultiMenu to operate on.
 * @item: The item to append.
 * 
 * Appends the given item to the q_multi_menu.
 **/
void
q_multi_menu_append_with_data (QMultiMenu *menu,
			       int count,
			       char const*const*item,
			       gpointer *data)
{
	int i;
	int j = 0;
	menu->priv->items = g_renew (QMultiMenuItem, menu->priv->items, menu->priv->item_count + count);
	for (i = 0; i < count; i++) {
		if (!strncmp (item[i], menu->priv->prefix, menu->priv->prefix_length)) {
			menu->priv->items[menu->priv->item_count + j].string = g_strdup (item[i]);
			if (data)
				menu->priv->items[menu->priv->item_count + j].datum = data[i];
			else
				menu->priv->items[menu->priv->item_count + j].datum = NULL;
			j++;
			d(g_print ("%s appended\n", item[i]);)
		}
	}
	menu->priv->item_count += j;

	changed (menu);
}

void
q_multi_menu_set_maximum (QMultiMenu *menu,
			  int maximum)
{
	menu->priv->maximum = maximum;
	changed (menu);
}

void
q_multi_menu_set_prefix   (QMultiMenu       *menu,
			   const char       *prefix)
{
	g_return_if_fail (menu->priv->item_count == 0);

	if (menu->priv->prefix)
		g_free (menu->priv->prefix);
	if (prefix)
		menu->priv->prefix = g_strdup (prefix);
	else
		menu->priv->prefix = g_strdup ("");
	menu->priv->prefix_length = strlen (prefix);
}

void
q_multi_menu_freeze (QMultiMenu *menu)
{
	if (menu->priv->frozen_count == 0 && menu->priv->idle_id) {
		g_source_remove (menu->priv->idle_id);
		menu->priv->idle_id = 0;
	}
	menu->priv->frozen_count ++;
}

void
q_multi_menu_thaw (QMultiMenu *menu)
{
	menu->priv->frozen_count --;
	if (menu->priv->frozen_count == 0 && menu->priv->changed) {
		changed (menu);
	}
}
