/* 
 *
 * Authors:  Ben Kahn
 */
#ifndef __FILE_OP_H__
#define __FILE_OP_H__

void exec_application (char *cmd, char *args, char *name, gboolean need_term);

#endif /* __FILE_OP_H__ */
