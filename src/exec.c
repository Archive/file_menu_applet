/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <gnome.h>
#include <applet-widget.h>
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include "main.h"

void 
exec_document (void *file_path)
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *file_info;
	GnomeVFSMimeApplication *application;

	gint term_argc;
	gchar **term_argv;
	gchar *term = NULL, *arg = NULL;

	GPtrArray *uarray;

	char *xalf = NULL;
	char **options_argv = NULL;

	int envc = 0;
	char **envp = NULL;	    

	uarray = g_ptr_array_new();

	/* Detect the file type. */
	
	file_info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info ((char *) file_path, file_info, (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
								 | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
	if (result != GNOME_VFS_OK) {
		fprintf (stderr, "file_menu_applet: %s: %s\n",
			 (char *)file_path, gnome_vfs_result_to_string (result));
		return;
	}
	
	/* Find application */
	application = gnome_vfs_mime_get_default_application ((char *)file_info->mime_type);


	/* Get any terminal settings we need. */
	if (application->requires_terminal) {
		gnome_config_get_vector ("/Gnome/Applications/Terminal",
					 &term_argc, &term_argv);
		if (term_argv == NULL) {
			
			if (gnome_is_program_in_path ("gnome-terminal")) {
				term = g_strdup ("gnome-terminal");
				arg = g_strdup ("-x");
			} else {
				term = g_strdup ("xterm");
				arg = g_strdup ("-e");
			}
		} else {
			term = term_argv[0];
			arg = term_argv[1];
		}
	}
	
	/* Find out the XALF status */
	
	if (gnome_config_get_bool("/xalf/settings/enabled=true")) {
		xalf = gnome_is_program_in_path ("xalf");

		if (xalf != NULL) {
			int options_argc = 0;
			int i;
			
 			g_ptr_array_add (uarray, (gpointer) xalf);
 			g_ptr_array_add (uarray, (gpointer) "--title");
 			g_ptr_array_add (uarray, (gpointer) application->name);

			gnome_config_get_vector ("/xalf/settings/options",
						 &options_argc, &options_argv);

			if (options_argc > 0 &&
			    options_argv != NULL) {
				for (i = 0; i < options_argc; i++) 
					g_ptr_array_add (uarray,
							 options_argv[i]);
			}
		}
	}
	
 	g_ptr_array_add (uarray, (gpointer) "/bin/sh");
 	g_ptr_array_add (uarray, (gpointer) "-c");

	g_ptr_array_add (uarray, 
			 (gpointer) g_strdup_printf ("%s %s %s \"%s\"", 
						     application->requires_terminal ? term : "" , 
						     application->requires_terminal ? arg : "",
						     application->command,
						     (char *) file_path));

	printf ("%s %s %s %s\n", 
		application->requires_terminal ? term : "" , 
		application->requires_terminal ? arg : "",
		application->command,
		(char *) file_path);
	
	gnome_execute_async_with_env (NULL, uarray->len, (char **) uarray->pdata, envc, envp);
}

