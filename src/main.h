#include "q-multi-menu.h"

/*
extern GtkWidget *item;
extern GtkWidget *applet;
extern GtkWidget *frame;
extern gchar *gconf_key;
extern gchar *gconf_key_menu;
extern gchar *gconf_key_dir;
extern gchar *gconf_key_icon;
*/

enum itype {file, directory};

#define GCONF_KEY "/apps/file_menu_applet/"
#define NAUTILUS_KEY "/apps/nautilus"

#define N_PREF_DIRFIRST "/preferences/sort_directories_first"
#define N_PREF_SHOWDOT "/preferences/show_hidden_files"
#define N_PREF_SHOWBACK "/preferences/show_backup_files"
#define N_PREF_THEME "/preferences/theme"

typedef struct _directory_menu directory_menu;
typedef struct _menu_item menu_item;
typedef struct _mime_in_dir mime_in_dir;
typedef struct _file_menu_applet file_menu_applet;

struct _directory_menu {
  time_t      mtime;         /* The last mtime we saw.  If this ever changes, invalidate the menu and all submenus. */
  char       *name;
  GtkWidget  *dmenu;
  GtkWidget  *tearoff;
  QMultiMenu *multimenu;
  GTimer     *timer;
  gulong      last_time;
};

struct _menu_item {
  enum itype      type;
  char      *name;
  char      *path;
  /*  GtkWidget *item; */
};

struct _mime_in_dir {
  char      *mime_type;
  char      *dir;
};

/* The main applet structure. */
struct _file_menu_applet {
  GtkWidget *applet;

  GHashTable *directories;

  GtkWidget *menu_bar;
  directory_menu *dm;
  GtkWidget *documents_item;
  GtkWidget *action_menu;

  GtkTooltips *menu_tips;

  GConfClient *client;

  /* Local Preferences */
  gchar *menu_label;
  gchar *dir_watch;
  gboolean icon_menus;

  /* Our keys. */
  gchar *gconf_key;
  gchar *gconf_key_menu;
  gchar *gconf_key_dir;
  gchar *gconf_key_icon;

  gboolean show_dir_first;
  gboolean show_dot_files;
  gboolean show_bak_files;
  gchar *icon_theme;

  GladeXML *preferences;

};

extern file_menu_applet *fma;

void doabout(AppletWidget * widget, gpointer data);
void show_directory_menu (GtkWidget *menu, void *path);
GtkWidget *pixmap_menu_item_new (const char *text, const GdkPixbuf *icon, int truncate);
GtkWidget *create_document_menu               (GtkWidget *pmenu);
gboolean maybe_popup_dir_menu (GtkWidget *widget, GdkEventButton *event, void *parent_menu);
gboolean maybe_popup_menu (GtkWidget *widget, GdkEventButton *event, void *file_path);
void exec_document (void *file_path);
void exec_terminal (gchar *path);
void exec_fm_wrap (GtkWidget *widget, void *path);
void exec_terminal_wrap (GtkWidget *widget, void *path);
void remove_old_menus (GtkWidget *item, gpointer menu);
void show_preferences_menu ();

void set_drag_stuff_on_file_menu (GtkWidget *menuitem, gchar *full_path);

void exec_document_wrap                       (GtkWidget *widget, 
					       void *parent_menu);
void set_drag_stuff_on_directory_menu         (GtkWidget *menuitem, gchar *full_path);
void set_drag_stuff_on_menu_bar               (GtkWidget *bar);

void show_preferences_menu ();
void set_menu_title                           (GConfClient* client,
		                               guint cnxn_id,
					       GConfEntry *entry,
					       gpointer user_data);
void change_watched_directory                 (GConfClient* client,
					       guint cnxn_id,
					       GConfEntry *entry,
					       gpointer user_data);
void change_icon_setting                      (GConfClient* client,
					       guint cnxn_id,
					       GConfEntry *entry,
					       gpointer user_data);


void show_rename_dialog (GtkWidget *menu, gchar *file_name);
void show_move_dialog (GtkWidget *menu, gchar *file_name);
void show_copy_dialog (GtkWidget *menu, gchar *file_name);
void delete_file (GtkWidget *w, GtkWidget *fs );
void show_handler (GtkWidget *w, char *fs);
gchar *translate_directory_name (const gchar *dir);
char *do_truncate (const char *text, int truncate);
void start_app_in_dir (GtkWidget *w, void *mid);
void create_new_directory_menu (directory_menu *dm);
void create_dir_dialog (GtkWidget *menu, gchar *full_path);
void create_dir (GtkWidget *name_dialog, int bn, gchar *full_path);
