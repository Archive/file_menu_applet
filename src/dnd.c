/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <gnome.h>
#include <applet-widget.h>
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <gconf/gconf-client.h>
#include <glade/glade.h>

#include "main.h"

extern GtkWidget *documents_item;
static void drag_data_get_menu_cb (GtkWidget *widget, GdkDragContext *context, 
				   GtkSelectionData *selection_data, guint info, 
				   guint time, char *item_loc);

/* This is the most horrible thing I can do.
   It can break at any time with an API compatible 
   change to GTK+ and there is nothing I can do about it.
*/
typedef struct _MyGtkMenu	      MyGtkMenu;
typedef struct _MyGtkMenuItem	      MyGtkMenuItem;

struct _MyGtkMenu
{
  GtkMenuShell menu_shell;
  
  GtkWidget *parent_menu_item;
  GtkWidget *old_active_menu_item;
  
  GtkAccelGroup *accel_group;
  GtkMenuPositionFunc position_func;
  gpointer position_func_data;

  /* Do _not_ touch these widgets directly. We hide the reference
   * count from the toplevel to the menu, so it must be restored
   * before operating on these widgets
   */
  GtkWidget *toplevel;
  GtkWidget *tearoff_window;

  guint torn_off : 1;
};

struct _MyGtkMenuItem
{
  GtkItem item;
  
  GtkWidget *submenu;
  
  guint	  accelerator_signal;
  guint16 toggle_size;
  guint16 accelerator_width;
  
  guint show_toggle_indicator : 1;
  guint show_submenu_indicator : 1;
  guint submenu_placement : 1;
  guint submenu_direction : 1;
  guint right_justify: 1;
  guint timer;
};


#if 0
/* This is a _horrible_ hack to have this here. This needs to be added to the
 * GTK+ menuing code in some manner.
 */
static void  
drag_end_menu_cb (GtkWidget *widget, GdkDragContext     *context)
{
  GtkWidget *xgrab_shell;
  GtkWidget *parent;

  /* Find the last viewable ancestor, and make an X grab on it
   */
  parent = widget->parent;
  xgrab_shell = NULL;
  while (parent)
    {
      gboolean viewable = TRUE;
      GtkWidget *tmp = parent;
      
      while (tmp)
	{
	  if (!GTK_WIDGET_MAPPED (tmp))
	    {
	      viewable = FALSE;
	      break;
	    }
	  tmp = tmp->parent;
	}
      
      if (viewable)
	xgrab_shell = parent;
      
      parent = GTK_MENU_SHELL (parent)->parent_menu_shell;
    }
  
  if (xgrab_shell && !GTK_MENU(xgrab_shell)->torn_off)
    {
      GdkCursor *cursor = gdk_cursor_new (GDK_ARROW);

      if ((gdk_pointer_grab (xgrab_shell->window, TRUE,
			     GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
			     GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK |
			     GDK_POINTER_MOTION_MASK,
			     NULL, cursor, GDK_CURRENT_TIME) == 0))
	{
	  if (gdk_keyboard_grab (xgrab_shell->window, TRUE,
				 GDK_CURRENT_TIME) == 0)
	    GTK_MENU_SHELL (xgrab_shell)->have_xgrab = TRUE;
	  else
	    {
	      gdk_pointer_ungrab (GDK_CURRENT_TIME);
	    }
	}

      gdk_cursor_destroy (cursor);
    }
}
#endif

static void  
target_drag_data_received  (GtkWidget          *widget,
                            GdkDragContext     *context,
                            gint                x,
                            gint                y,
                            GtkSelectionData   *data,
                            guint               info,
                            guint               time)
{
	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	gtk_drag_finish (context, TRUE, FALSE, time);

        g_print("Got: %s into %s\n", 
		data->data, 
		(char *) gtk_object_get_data (GTK_OBJECT (widget), "full_path"));
	
}

static gint 
popdown_menu (gpointer widget)
{
	printf ("Popping down %s\n", 
		(char *) gtk_object_get_data (GTK_OBJECT (widget), "full_path"));
	
	gtk_object_set_data (GTK_OBJECT (widget), "popdown_timer", 0);
	gtk_object_set_data (GTK_OBJECT (widget), "popped_up", 0);
	
	return FALSE;
}

static gint
popup_menu (gpointer widget)
{

	if (!gtk_object_get_data (GTK_OBJECT (widget), "popped_up")) {

		printf ("Popping up %s\n", 
			(char *) gtk_object_get_data (GTK_OBJECT (widget), "full_path"));

		gtk_object_set_data (GTK_OBJECT (widget), "popped_up", (gpointer) TRUE);
		gtk_signal_emit_by_name (widget, "activate-item", NULL);
		/*
		submenu = ((MyGtkMenuItem *)GTK_MENU_ITEM(widget))->submenu;
		gtk_grab_add (submenu);
		*/
		gtk_signal_connect (GTK_OBJECT (widget), "deselect",
				    GTK_SIGNAL_FUNC (popdown_menu),
				    NULL);

	}
	
	gtk_object_set_data (GTK_OBJECT (widget), "popup_timer", 0);
	return FALSE;
}

static gboolean  
print_motion_stuff         (GtkWidget          *widget,
                            GdkDragContext     *context,
                            gint                x,
                            gint                y,
                            guint               time)
{

	if (!gtk_object_get_data (GTK_OBJECT (widget), "popup_timer")) {
		gtk_object_set_data (GTK_OBJECT (widget), "popup_timer", 
				     (gpointer) gtk_timeout_add (333, popup_menu, (gpointer) widget));
	}

	return TRUE;
}

static void
print_leave_stuff         (GtkWidget           *widget,
			   GdkDragContext      *context,
			   guint                time)
{
	gint timer = (gint) gtk_object_get_data (GTK_OBJECT (widget), "popup_timer");
	if (timer) {
		gtk_timeout_remove (timer);
		gtk_object_set_data (GTK_OBJECT (widget), "popup_timer", 0);
	}}


static void  
drag_data_get_menu_cb (GtkWidget *widget, GdkDragContext     *context,
		       GtkSelectionData   *selection_data, guint info,
		       guint time, char *item_loc)
{
	gchar *uri_list = g_strconcat ("file:", item_loc, "\r\n", NULL);
	printf ("Item to drag: %s\n", item_loc);
	gtk_selection_data_set (selection_data,
				selection_data->target, 8, (guchar *)uri_list,
				strlen(uri_list));
	g_free(uri_list);
}

void
set_drag_stuff_on_file_menu (GtkWidget *menuitem, gchar *full_path)
{

        static GtkTargetEntry menu_item_targets[] = {
		{ "text/uri-list",               0, 0 },
		{ "x-special/gnome-icon-list", 0, 1 }
	};
	

	/* You can copy a file to another application... */
	gtk_drag_source_set(menuitem,
			    GDK_BUTTON1_MASK|GDK_BUTTON3_MASK,
			    menu_item_targets, 2,
			    GDK_ACTION_COPY);
	
	/* Copy To... */
	gtk_signal_connect(GTK_OBJECT(menuitem), "drag_data_get",
			   drag_data_get_menu_cb,
			   (gpointer)full_path);

	/* Receive From... */
	gtk_drag_dest_set (GTK_WIDGET (menuitem),
			   GTK_DEST_DEFAULT_ALL,
			   menu_item_targets, 2,
			   GDK_ACTION_COPY);
	gtk_signal_connect (GTK_OBJECT (menuitem), "drag_data_received",
			    GTK_SIGNAL_FUNC (target_drag_data_received),
			    NULL);
	
}

void
set_drag_stuff_on_directory_menu (GtkWidget *menuitem, gchar *full_path)
{

        static GtkTargetEntry menu_item_targets[] = {
		{ "text/uri-list",               0, 0 },
		{ "x-special/gnome-icon-list", 0, 1 }
	};
	

	/* You can drag a directory somewhere... */
	gtk_drag_source_set(menuitem,
			    GDK_BUTTON1_MASK|GDK_BUTTON3_MASK,
			    menu_item_targets, 2,
			    GDK_ACTION_COPY);
	gtk_signal_connect(GTK_OBJECT(menuitem), "drag_data_get",
			   drag_data_get_menu_cb,
			   (gpointer)full_path);
	
	/* We need to know to pop up a sub directory. */
	gtk_signal_connect(GTK_OBJECT(menuitem), "drag_motion",
			   GTK_SIGNAL_FUNC (print_motion_stuff), NULL);
	gtk_signal_connect(GTK_OBJECT(menuitem), "drag_leave",
			   GTK_SIGNAL_FUNC (print_leave_stuff), NULL);


	/* You can drop a file onto a directory... */
	gtk_drag_dest_set (GTK_WIDGET (menuitem),
			   GTK_DEST_DEFAULT_ALL,
			   menu_item_targets, 2,
			   GDK_ACTION_COPY);
	gtk_signal_connect (GTK_OBJECT (menuitem), "drag_data_received",
			    GTK_SIGNAL_FUNC (target_drag_data_received),
			    NULL);
	
}

void
set_drag_stuff_on_menu_bar (GtkWidget *bar)
{

        static GtkTargetEntry menu_item_targets[] = {
		{ "text/uri-list",               0, 0 },
		{ "x-special/gnome-icon-list", 0, 1 }
	};
	
	/* We only accept drops here.  No drags. */
	
	/* We need to know when to pop up. */
	gtk_signal_connect(GTK_OBJECT(bar), "drag_motion",
			   GTK_SIGNAL_FUNC (print_motion_stuff), NULL);
	gtk_signal_connect(GTK_OBJECT(bar), "drag_leave",
			   GTK_SIGNAL_FUNC (print_leave_stuff), NULL);

	/* But we also accept drag on the main menubutton. */
	gtk_drag_dest_set (GTK_WIDGET(bar),
			   GTK_DEST_DEFAULT_ALL,
			   menu_item_targets, 2,
			   GDK_ACTION_COPY);

	/* We need to know when we get something.  :^) */
	gtk_signal_connect (GTK_OBJECT (bar), "drag_data_received",
			    GTK_SIGNAL_FUNC (target_drag_data_received),
			    NULL);

}
