/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <gnome.h>
#include <applet-widget.h>
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>

#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include "main.h"
#include "fileop.h"

extern GtkWidget *documents_menu;
extern GtkWidget *documents_item;

void file_copy_sel( GtkWidget        *w,
		    gpointer         *fn);
void file_move_sel( GtkWidget        *w,
		    gpointer         *s);
void file_rename_sel( GtkWidget        *w,
		      gchar *fs );
GnomeVFSURI *get_real_uri (const char *path);
static gint xfer_progress_callback (GnomeVFSXferProgressInfo *info,
				    gpointer data);

GtkWidget *make_status_popup (char *operation);
void set_status_on_popup (GtkWidget *popup, char *status);
void destroy_status_popup (GtkWidget *popup);
void file_error_message (char *title, char *message);
gboolean destroy_status_popup2 (gpointer data);


void
exec_application (char *cmd, char *args, char *name, gboolean needs_term)
{
	gint term_argc;
	gchar **term_argv;
	gchar *term = NULL, *arg = NULL;

	GPtrArray *uarray;

	char *xalf = NULL;
	char **options_argv = NULL;

	int envc = 0;
	char **envp = NULL;	    

	uarray = g_ptr_array_new();

	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	/* Get any terminal settings we need. */
	if (needs_term) {
		gnome_config_get_vector ("/Gnome/Applications/Terminal",
					 &term_argc, &term_argv);
		if (term_argv == NULL) {
			
			if (gnome_is_program_in_path ("gnome-terminal")) {
				term = g_strdup ("gnome-terminal");
				arg = g_strdup ("-x");
			} else {
				term = g_strdup ("xterm");
				arg = g_strdup ("-e");
			}
		} else {
			term = term_argv[0];
			arg = term_argv[1];
		}
	}
	
	/* Find out the XALF status */
	
	if (gnome_config_get_bool("/xalf/settings/enabled=true")) {
		xalf = gnome_is_program_in_path ("xalf");

		if (xalf != NULL) {
			int options_argc = 0;
			int i;
			
 			g_ptr_array_add (uarray, (gpointer) xalf);
 			g_ptr_array_add (uarray, (gpointer) "--title");
 			g_ptr_array_add (uarray, (gpointer) name);

			gnome_config_get_vector ("/xalf/settings/options",
						 &options_argc, &options_argv);

			if (options_argc > 0 &&
			    options_argv != NULL) {
				for (i = 0; i < options_argc; i++) 
					g_ptr_array_add (uarray,
							 options_argv[i]);
			}
		}
	}
	
 	g_ptr_array_add (uarray, (gpointer) "/bin/sh");
 	g_ptr_array_add (uarray, (gpointer) "-c");


	if (args)
		g_ptr_array_add (uarray, 
				 (gpointer) g_strdup_printf ("%s %s %s \"%s\"", 
							     needs_term ? term : "" , 
							     needs_term && cmd? arg : "",
							     cmd ? cmd : "",
							     (char *) args));
	else
		g_ptr_array_add (uarray, 
				 (gpointer) g_strdup_printf ("%s %s %s", 
							     needs_term ? term : "" , 
							     needs_term && cmd ? arg : "",
							     cmd ? cmd : ""));

	gnome_execute_async_with_env (NULL, uarray->len, (char **) uarray->pdata, envc, envp);


}

void 
exec_document (void *file_path)
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *file_info;
	GnomeVFSMimeApplication *application;


	/* Detect the file type. */
	
	file_info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info ((char *) file_path, file_info, (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
								 | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
	if (result != GNOME_VFS_OK) {
		fprintf (stderr, "file_menu_applet: %s: %s\n",
			 (char *)file_path, gnome_vfs_result_to_string (result));
		return;
	}
	
	/* Find application */
	application = gnome_vfs_mime_get_default_application ((char *)file_info->mime_type);

	if (!application) {
		/* FIXME: We should ask about defining a default handler here. */
		printf ("*** WARNING ***: No handlers have been defined for %s!\n", (char *)file_info->mime_type);
		return;
	}

	exec_application (application->command, file_path, application->name, application->requires_terminal);
}


void
exec_terminal (gchar *path)
{
	
	chdir (path);
	exec_application (NULL, NULL, "Terminal", TRUE);

}

void
start_app_in_dir (GtkWidget *w, void *mid)
{
	mime_in_dir *md = mid;
	GnomeVFSMimeApplication *application;

	chdir (md->dir);
	application = gnome_vfs_mime_get_default_application ((char *)md->mime_type);
	exec_application (application->command, NULL, application->name, application->requires_terminal);

}

void
show_copy_dialog (GtkWidget *menu, gchar *file_name) {
	GtkWidget *filew;

	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	/* Maybe there are events waiting.  This will make it seem to multitask.*/
	while (gtk_events_pending ()) gtk_main_iteration ();  /* This should no longer be needed. */	

	filew = gtk_file_selection_new (g_strconcat ("Copy '", file_name, "' to ...", NULL));
 
	gtk_file_selection_set_filename (GTK_FILE_SELECTION(filew), file_name);
	
	/* Connect the ok_button to file_ok_sel function */
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filew)->ok_button),
			    "clicked", (GtkSignalFunc) file_copy_sel, file_name);
	
	/* Connect the cancel_button to destroy the widget */
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION
					       (filew)->cancel_button),
				   "clicked", (GtkSignalFunc) gtk_widget_destroy,
				   GTK_OBJECT (filew));
	
	gtk_widget_show(filew);

	return;
}

void
show_move_dialog (GtkWidget *menu, gchar *file_name) {
	GtkWidget *filew;

	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	filew = gtk_file_selection_new (g_strconcat ("Move (rename) '", file_name, "' to ...", NULL));
 
	gtk_file_selection_set_filename (GTK_FILE_SELECTION(filew), file_name);
	
	/* Connect the ok_button to file_ok_sel function */
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (filew)->ok_button),
			    "clicked", (GtkSignalFunc) file_move_sel, file_name);
	
	/* Connect the cancel_button to destroy the widget */
	gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION
					       (filew)->cancel_button),
				   "clicked", (GtkSignalFunc) gtk_widget_destroy,
				   GTK_OBJECT (filew));
	
	gtk_widget_show(filew);

	return;
}

void 
create_dir (GtkWidget *name_dialog, int bn, gchar *full_path)
{
	gchar *name;
	GtkWidget *entry;
	GnomeVFSResult result;

	entry = gtk_object_get_data (GTK_OBJECT (name_dialog), "entry1");

	name = gtk_entry_get_text (GTK_ENTRY (entry));

	result = gnome_vfs_make_directory (g_strconcat (full_path, "/", name, NULL), 0750);

	if (result != GNOME_VFS_OK) {
		file_error_message ("Error Creating Directory", "Could not Create Directory.\nMaybe bad filename or no permission?");
	}
}

void
create_dir_dialog (GtkWidget *menu, gchar *full_path) 
{
	GtkWidget *dir_create;
	GtkWidget *dialog_vbox2;
	GtkWidget *vbox4;
	GtkWidget *label13;
	GtkWidget *hbox1;
	GtkWidget *label14;
	GtkWidget *entry1;
	GtkWidget *dialog_action_area2;
	
	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	dir_create = gnome_dialog_new (_("Create a New Directory Named..."), GNOME_STOCK_BUTTON_CANCEL, GNOME_STOCK_BUTTON_OK, NULL);
	gtk_object_set_data (GTK_OBJECT (dir_create), "dir_create", dir_create);
	gtk_window_set_policy (GTK_WINDOW (dir_create), FALSE, FALSE, FALSE);
	gnome_dialog_set_close (GNOME_DIALOG (dir_create), TRUE);
	
	dialog_vbox2 = GNOME_DIALOG (dir_create)->vbox;
	gtk_object_set_data (GTK_OBJECT (dir_create), "dialog_vbox2", dialog_vbox2);
	gtk_widget_show (dialog_vbox2);
	
	vbox4 = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox4);
	gtk_object_set_data_full (GTK_OBJECT (dir_create), "vbox4", vbox4,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox4);
	gtk_box_pack_start (GTK_BOX (dialog_vbox2), vbox4, TRUE, TRUE, 0);
	
	label13 = gtk_label_new (_("Enter the directory name to create"));
	gtk_widget_ref (label13);
	gtk_object_set_data_full (GTK_OBJECT (dir_create), "label13", label13,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label13);
	gtk_box_pack_start (GTK_BOX (vbox4), label13, FALSE, FALSE, 0);
	gtk_misc_set_padding (GTK_MISC (label13), 0, 5);
	
	hbox1 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox1);
	gtk_object_set_data_full (GTK_OBJECT (dir_create), "hbox1", hbox1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox1);
	gtk_box_pack_start (GTK_BOX (vbox4), hbox1, FALSE, FALSE, 0);
	
	label14 = gtk_label_new (_("Directory Name:"));
	gtk_widget_ref (label14);
	gtk_object_set_data_full (GTK_OBJECT (dir_create), "label14", label14,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label14);
	gtk_box_pack_start (GTK_BOX (hbox1), label14, FALSE, FALSE, 0);
	
	entry1 = gtk_entry_new ();
	gtk_widget_ref (entry1);
	gtk_object_set_data_full (GTK_OBJECT (dir_create), "entry1", entry1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (entry1);
	gtk_box_pack_start (GTK_BOX (hbox1), entry1, TRUE, TRUE, 0);
	
	dialog_action_area2 = GNOME_DIALOG (dir_create)->action_area;
	gtk_object_set_data (GTK_OBJECT (dir_create), "dialog_action_area2", dialog_action_area2);
	gtk_widget_show (dialog_action_area2);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area2), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area2), 8);
	
	gtk_signal_connect (GTK_OBJECT (dir_create), "clicked", GTK_SIGNAL_FUNC (create_dir), (gpointer) g_strdup (full_path));

	gnome_dialog_run_and_close (GNOME_DIALOG (dir_create));
}

void
show_rename_dialog (GtkWidget *menu, gchar *file_name) 
{
	GtkWidget *window1;
	GtkWidget *vbox1;
	GtkWidget *vbox2;
	GtkWidget *label1;
	GtkWidget *entry1;
	GtkWidget *hbuttonbox1;
	GtkWidget *button7;
	GtkWidget *button8;
	GtkAccelGroup *accel_group;
	
	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	accel_group = gtk_accel_group_new ();
	
	window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_object_set_data (GTK_OBJECT (window1), "window1", window1);
	gtk_window_set_title (GTK_WINDOW (window1), _("Rename File To..."));
	
	vbox1 = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox1);
	gtk_object_set_data_full (GTK_OBJECT (window1), "vbox1", vbox1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (window1), vbox1);
	gtk_container_set_border_width (GTK_CONTAINER (vbox1), 5);
	
	vbox2 = gtk_vbox_new (FALSE, 5);
	gtk_widget_ref (vbox2);
	gtk_object_set_data_full (GTK_OBJECT (window1), "vbox2", vbox2,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, FALSE, 5);
	
	label1 = gtk_label_new (_("Rename File To,,,"));
	gtk_widget_ref (label1);
	gtk_object_set_data_full (GTK_OBJECT (window1), "label1", label1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label1);
	gtk_box_pack_start (GTK_BOX (vbox2), label1, FALSE, TRUE, 0);
	gtk_misc_set_alignment (GTK_MISC (label1), 7.45058e-09, 0.5);
	
	entry1 = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (entry1), file_name);
	gtk_widget_ref (entry1);
	gtk_object_set_data_full (GTK_OBJECT (window1), "entry1", entry1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (entry1);
	gtk_box_pack_start (GTK_BOX (vbox2), entry1, FALSE, FALSE, 0);
	gtk_widget_set_usize (entry1, 256, -2);
	
	hbuttonbox1 = gtk_hbutton_box_new ();
	gtk_widget_ref (hbuttonbox1);
	gtk_object_set_data_full (GTK_OBJECT (window1), "hbuttonbox1", hbuttonbox1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbuttonbox1);
	gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox1, FALSE, FALSE, 5);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox1), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbuttonbox1), 5);
	gtk_button_box_set_child_ipadding (GTK_BUTTON_BOX (hbuttonbox1), 0, 0);
	
	button7 = gnome_stock_button (GNOME_STOCK_BUTTON_CANCEL);
	gtk_widget_ref (button7);
	gtk_object_set_data_full (GTK_OBJECT (window1), "button7", button7,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (button7);
	gtk_container_add (GTK_CONTAINER (hbuttonbox1), button7);
	GTK_WIDGET_SET_FLAGS (button7, GTK_CAN_DEFAULT);
	gtk_widget_add_accelerator (button7, "clicked", accel_group,
				    GDK_Escape, 0,
				    GTK_ACCEL_VISIBLE);
	
	button8 = gnome_stock_button (GNOME_STOCK_BUTTON_OK);
	gtk_widget_ref (button8);
	gtk_object_set_data_full (GTK_OBJECT (window1), "button8", button8,
                            (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (button8);
	gtk_container_add (GTK_CONTAINER (hbuttonbox1), button8);
	GTK_WIDGET_SET_FLAGS (button8, GTK_CAN_DEFAULT);
	gtk_widget_add_accelerator (button8, "clicked", accel_group,
				    GDK_Return, 0,
				    GTK_ACCEL_VISIBLE);
	
	gtk_signal_connect_object (GTK_OBJECT (button7), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (window1));
	gtk_signal_connect (GTK_OBJECT (button8), "clicked",
			    GTK_SIGNAL_FUNC (file_rename_sel),
			    file_name);
	
	gtk_widget_grab_focus (entry1);
	gtk_window_add_accel_group (GTK_WINDOW (window1), accel_group);
	gtk_widget_show_all (window1);
}

/* Get the selected filename and print it to the console */
void file_copy_sel( GtkWidget        *w,
		    gpointer         *s) 
{
	GtkWidget *popup;
	GtkFileSelection *fs = GTK_FILE_SELECTION (gtk_widget_get_toplevel (w));
	GnomeVFSURI *src_uri, *dest_uri;
	GnomeVFSResult result;
	GnomeVFSXferOptions xfer_options;
	
	xfer_options = 0; /* Normal copy. */

	src_uri = get_real_uri ((gchar *) s);
	dest_uri = get_real_uri (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));

	popup = make_status_popup (g_strdup_printf ("Copying %s to %s\n", 
						    (gchar *) s, 
						    gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs))));
	
	gtk_widget_destroy (gtk_widget_get_toplevel(w));
	
	result = gnome_vfs_xfer_uri (src_uri, dest_uri,
				     xfer_options,
				     GNOME_VFS_XFER_ERROR_MODE_QUERY,
				     GNOME_VFS_XFER_OVERWRITE_MODE_QUERY,
				     xfer_progress_callback,
				     popup);
}

/* Get the selected filename and print it to the console */
void file_move_sel( GtkWidget        *w,
		    gpointer         *s) 
{
	GtkWidget *popup;
	GtkFileSelection *fs = GTK_FILE_SELECTION (gtk_widget_get_toplevel (w));
	GnomeVFSURI *src_uri, *dest_uri;
	GnomeVFSResult result;
	GnomeVFSXferOptions xfer_options;
	
	xfer_options = GNOME_VFS_XFER_REMOVESOURCE; /* Normal move. */

	src_uri = get_real_uri ((gchar *) s);
	dest_uri = get_real_uri (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));

	popup = make_status_popup (g_strdup_printf ("Moving %s to %s\n", 
						    (gchar *) s, 
						    gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs))));
	
	gtk_widget_destroy (gtk_widget_get_toplevel(w));
	
	result = gnome_vfs_xfer_uri (src_uri, dest_uri,
				     xfer_options,
				     GNOME_VFS_XFER_ERROR_MODE_QUERY,
				     GNOME_VFS_XFER_OVERWRITE_MODE_QUERY,
				     xfer_progress_callback,
				     popup);

	/*	g_print ("Would have moved %s to %s\n", (gchar *) s, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));*/
	gtk_widget_destroy (gtk_widget_get_toplevel(w));

}

/* Get the selected filename and print it to the console */
void file_rename_sel( GtkWidget        *w,
		      gchar            *fs ) 
{
	GtkWidget *entry;
	gchar *new_filename;

	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	entry = gtk_object_get_data (GTK_OBJECT (gtk_widget_get_toplevel(w)), "entry1");
	new_filename = gtk_entry_get_text (GTK_ENTRY (entry));

	g_print ("Would have renamed %s to %s\n", fs, new_filename);
	gtk_widget_destroy (gtk_widget_get_toplevel(w));
}

void 
delete_file (GtkWidget *w,
		  GtkWidget      *pmenu)
{
	gchar *filename;

	/* We need to get rid of the GTK menu... */
	/* gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL); */

	/* Need filename and menu item */
	filename = (gchar *) gtk_object_get_data (GTK_OBJECT (pmenu), "full_path");
	gtk_widget_hide (pmenu);

	gnome_vfs_unlink_from_uri (get_real_uri (filename));

	return;
}

void 
show_handler (GtkWidget *w,
	      char *fs)
{
	/* We need to get rid of the GTK menu... */
	gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);

	exec_application ("file-types-capplet", fs, "File Type Editor", FALSE);
	return;
}

gchar *
translate_directory_name (const gchar *dir)
{
	gchar *home, *found, *ret;

	home = g_get_home_dir ();
	
	if (!dir) return strdup (home);
	ret = strdup (dir);

	if (!home)
		home = strdup ("/");

	if (!dir)
		return home;

	/* We're searching for three forms:
	   $HOME anywhere in the string translates to: getenv ("HOME");
	   ^~/ translates to getenv ("HOME");
	   ^user/ translates to getpwnam(user);
	   Otherwise, we just return.
	*/

	while ((found = strstr (ret, "$HOME"))) {
		gchar *tmp;

		found[0] = '\0';
		tmp = g_strconcat (ret, home, "/", found + 5, NULL);
		g_free (ret);
		ret = tmp;
	}

	if (ret[0] == '~') {
		gchar *first_slash;
		gint cmp_len;

		first_slash = strchr(ret, '/');
		if (first_slash)
			cmp_len = first_slash - ret;
		else
			cmp_len = strlen(ret + 1);

		if ((!cmp_len) || (cmp_len == 1)) { 
			/* ~/ */
			gchar *tmp;
			
			tmp = g_strconcat (home, "/", ret + 2, NULL);
			g_free (ret);
			ret = tmp;
		} else {

			/* ~user/ */
			gchar *tmp, *ud;
			struct passwd *user;
			
			ret[cmp_len] = '\0';

			user = getpwnam(ret + 1);
			if (!user) {
				ud = strdup ("/");
			} else {
				ud = user->pw_dir;
			}

			tmp = g_strconcat (ud, "/", ret + cmp_len + 1, NULL);
			g_free (ret);
			ret = tmp;
		}
	}
	/*	printf ("Got: %s from %s\n", ret, dir);*/
	return ret;
}

GnomeVFSURI *
get_real_uri (const char *path)
{
	if (strstr (path, "://")) {
		return gnome_vfs_uri_new (path);
	}
	return gnome_vfs_uri_new (gnome_vfs_get_uri_from_local_path (path));
}

GtkWidget *
make_status_popup (char *operation)
{
	GtkWidget *popup;
	GtkWidget *vbox;
	GtkWidget *label1;
	GtkWidget *label2;

	popup = gtk_window_new (GTK_WINDOW_DIALOG);
	gtk_window_set_title (GTK_WINDOW (popup), _("Please Wait..."));
	gtk_object_set_data_full (GTK_OBJECT (popup), "popup", popup,
				  (GtkDestroyNotify) gtk_widget_unref);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_widget_ref (vbox);
	gtk_widget_show (vbox);
	gtk_object_set_data_full (GTK_OBJECT (popup), "vbox", vbox,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_container_add (GTK_CONTAINER (popup), vbox);

	label1 = gtk_label_new (operation);
	gtk_widget_ref (label1);
	gtk_widget_show (label1);
	gtk_object_set_data_full (GTK_OBJECT (popup), "label1", label1,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_box_pack_start (GTK_BOX (vbox), label1, FALSE, TRUE, 0);

	label2 = gtk_label_new (_("Status"));
	gtk_widget_ref (label2);
	gtk_widget_show (label2);
	gtk_object_set_data_full (GTK_OBJECT (popup), "label2", label2,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_box_pack_start (GTK_BOX (vbox), label2, FALSE, TRUE, 0);

	gtk_widget_show_all (popup);

	return popup;
	
}

void
set_status_on_popup (GtkWidget *popup, char *status)
{
	GtkWidget *label;

	label = gtk_object_get_data (GTK_OBJECT (popup), "label2");
	gtk_label_set_text (GTK_LABEL (label), status);
}

void
destroy_status_popup (GtkWidget *popup)
{
	gtk_timeout_add (1000, destroy_status_popup2, popup);
}

gboolean
destroy_status_popup2 (gpointer data)
{
	GtkWidget *popup = data;
	printf ("Destroying!\n");
	gtk_widget_destroy (popup);
	return FALSE;
}

void
file_error_message (char *title, char *message)
{
	GtkWidget *error;
	GtkWidget *label;

	error = gnome_dialog_new (title,
				  GNOME_STOCK_BUTTON_OK,
				  NULL);
	label = gtk_label_new (message);
        gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (error)->vbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	gnome_dialog_run_and_close (GNOME_DIALOG (error));

	return;
}

static gint 
xfer_progress_callback (GnomeVFSXferProgressInfo *info,
			gpointer data)
{
	GtkWidget *popup = (GtkWidget *) data;

	switch (info->status) {
	case GNOME_VFS_XFER_PROGRESS_STATUS_VFSERROR:
		file_error_message ("GNOME-VFS Error",
				    g_strdup (gnome_vfs_result_to_string (info->vfs_status)));
		destroy_status_popup (popup);
		return FALSE;
		break;
	case GNOME_VFS_XFER_PROGRESS_STATUS_OVERWRITE:
		set_status_on_popup (popup, 
				     g_strdup_printf ("Overwriting `%s' with `%s'\n",
						      info->target_name, info->source_name));
		return FALSE;
		break;
	case GNOME_VFS_XFER_PROGRESS_STATUS_OK:
		switch (info->phase) {
		case GNOME_VFS_XFER_PHASE_INITIAL:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Initial phase\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_COLLECTING:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Collecting file list\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_READYTOGO:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Ready to go!\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_OPENSOURCE:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Opening source\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_OPENTARGET:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Opening target\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_COPYING:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Transferring `%s' to `%s' (file %ld/%ld, byte %ld/%ld in file, "
				"%" GNOME_VFS_SIZE_FORMAT_STR "/%" GNOME_VFS_SIZE_FORMAT_STR " total)\n",
				info->source_name,
				info->target_name,
				info->file_index,
				info->files_total,
				(glong) info->bytes_copied,
				(glong) info->file_size,
				info->total_bytes_copied,
				info->bytes_total));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_CLOSESOURCE:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Closing source\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_CLOSETARGET:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Closing target\n"));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_FILECOMPLETED:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Done with `%s' -> `%s', going next\n",
				info->source_name, info->target_name));
			return TRUE;
		case GNOME_VFS_XFER_PHASE_COMPLETED:
			destroy_status_popup (popup);
			return TRUE;
		default:
			set_status_on_popup (popup, 
				     g_strdup_printf ("Unexpected phase %d\n", info->phase));
			return TRUE; /* keep going anyway */
		}
	case GNOME_VFS_XFER_PROGRESS_STATUS_DUPLICATE:
		break;

		
	}

	return FALSE;
}
