#include <libgnome/gnome-defs.h>
#include <gtk/gtkmenuitem.h>

#ifndef __TITLE_ITEM_H__
#define __TITLE_ITEM_H__

BEGIN_GNOME_DECLS

/*
 * Type checking and casting macros
 */
#define TYPE_TITLE_ITEM	(title_item_get_type())
#define TITLE_ITEM(obj)	GTK_CHECK_CAST((obj), title_item_get_type(), TitleItem)
#define TITLE_ITEM_CONST(obj)	GTK_CHECK_CAST((obj), title_item_get_type(), TitleItem const)
#define TITLE_ITEM_CLASS(klass)	GTK_CHECK_CLASS_CAST((klass), title_item_get_type(), TitleItemClass)
#define IS_TITLE_ITEM(obj)	GTK_CHECK_TYPE((obj), title_item_get_type ())
#define PARENT_TYPE (gtk_menu_item_get_type())

/*
 * Main object structure
 */
typedef struct _TitleItem TitleItem;
typedef struct _TitleItemClass TitleItemClass;

struct _TitleItem {
	GtkMenuItem parent;
};

/*
 * Class definition
 */
struct _TitleItemClass {
	GtkMenuItemClass parent;
};

/*
 * Public methods
 */
guint      title_item_get_type  (void);
GtkWidget *title_item_new       (void);

END_GNOME_DECLS

#endif /* __TITLE_ITEM_H__ */
