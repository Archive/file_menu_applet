/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright 2002, Chris Lahey
 *
 * Authors:
 *   Chris Lahey <clahey@ximian.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License, version 2, as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef _Q_MULTI_MENU_H_
#define _Q_MULTI_MENU_H_

#include <gtk/gtkobject.h>
#include <libgnome/gnome-defs.h>

BEGIN_GNOME_DECLS

#define Q_MULTI_MENU_TYPE        (q_multi_menu_get_type ())
#define Q_MULTI_MENU(o)          (GTK_CHECK_CAST ((o), Q_MULTI_MENU_TYPE, QMultiMenu))
#define Q_MULTI_MENU_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), Q_MULTI_MENU_TYPE, QMultiMenuClass))
#define Q_IS_MULTI_MENU(o)       (GTK_CHECK_TYPE ((o), Q_MULTI_MENU_TYPE))
#define Q_IS_MULTI_MENU_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), Q_MULTI_MENU_TYPE))

typedef struct QMultiMenuPrivate_ QMultiMenuPrivate;

typedef struct {
	GtkObject   base;

	QMultiMenuPrivate *priv;
} QMultiMenu;

typedef struct {
	GtkObjectClass parent_class;

	/*
	 * Signals
	 */

	void (*changed)       (QMultiMenu *menu);
	void (*list_changed)  (QMultiMenu *menu, int list_idx);
	void (*list_inserted) (QMultiMenu *menu, int list_idx, int pos, int count);
	void (*list_deleted)  (QMultiMenu *menu, int list_idx, int pos, int count);

} QMultiMenuClass;


/* Standard methods */
GtkType     q_multi_menu_get_type          (void);
QMultiMenu *q_multi_menu_new               (void);

/* Inspection */
int         q_multi_menu_count             (QMultiMenu       *menu);
const char *q_multi_menu_list_label        (QMultiMenu       *menu,
					    int               list_idx);
int         q_multi_menu_list_length       (QMultiMenu       *menu,
					    int               list_idx);
const char *q_multi_menu_list_nth          (QMultiMenu       *menu,
					    int               list_idx,
					    int               n);
gpointer    q_multi_menu_list_nth_datum    (QMultiMenu       *menu,
					    int               list_idx,
					    int               n);

/* Modification */
void        q_multi_menu_append            (QMultiMenu       *menu,
					    int               count,
					    char const*const *item);
/* parallel arrays */
void        q_multi_menu_append_with_data  (QMultiMenu       *menu,
					    int               count,
					    char const*const *items,
					    gpointer         *data);
/* You must not call prefix after ever calling append. */
void        q_multi_menu_set_prefix        (QMultiMenu       *menu,
					    const char       *prefix);
/* Call this any time. */
void        q_multi_menu_set_maximum       (QMultiMenu       *menu,
					    int               maximum);

void        q_multi_menu_freeze (QMultiMenu *menu);
void        q_multi_menu_thaw (QMultiMenu *menu);

END_GNOME_DECLS

#endif /* _Q_MULTI_MENU_H_ */
