/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* 
 *** file menu applet
 *** Copyright (C) 2001 Benjamin Kahn <xkahn@ximian.com>
 *** 
 *** This program is free software; you can redistribute it and/or
 *** modify it under the terms of the GNU General Public License
 *** as published by the Free Software Foundation; either version 2
 *** of the License, or (at your option) any later version.
 *** 
 *** This program is distributed in the hope that it will be useful,
 *** but WITHOUT ANY WARRANTY; without even the implied warranty of
 *** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *** GNU General Public License for more details.
 *** 
 *** You should have received a copy of the GNU General Public License
 *** along with this program; if not, write to the Free Software
 *** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 *** 02111-1307, USA.
 ***/

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdkx.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <applet-widget.h>
#include <gnome.h>
#include <config.h>

#include <glade/glade.h>
#include <gconf/gconf-client.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include "main.h"
#include "scroll-menu.h"
#include "gicon.h"
#include "title-item.h"
#include "fileop.h"
#include "flist-mime-icon.h"

file_menu_applet *fma = NULL;

extern gchar *nautilus_theme;

/*
GtkWidget *applet;
GtkWidget *menu_bar;
GtkWidget *documents_menu;
GtkWidget *action_menu = NULL;
GtkTooltips *menu_tips;
gboolean icon_menus;
gchar *gconf_key = NULL;
gchar *gconf_key_menu = NULL;
gchar *gconf_key_dir = NULL;
gchar *gconf_key_icon = NULL;

GladeXML *preferences;

GHashTable *directories;
*/

void create_new_file_menuitem (GnomeVFSFileInfo *item, 
			       directory_menu *dm, 
			       char *full_path, 
			       char *mime_type);
void create_new_directory_submenu (GnomeVFSFileInfo *item, 
				   directory_menu *dm, 
				   char *full_path);
static gboolean load_directory_contents (const gchar *rel_path, 
					 GnomeVFSFileInfo *file_info,
					 gboolean recursing_will_loop,
					 gpointer data, 
					 gboolean *recurse);

void doabout(AppletWidget * widget, gpointer data)
{
	
	gchar *authors[] = {
	        "Benjamin Kahn <xkahn@ximian.com>",
		NULL
	};
	
	gtk_widget_show(gnome_about_new(PACKAGE, VERSION,
					"Copyright 2001, 2002",
					(const gchar **) authors,
					"File Manager Menu Applet", NULL));

}

char *
do_truncate (const char *text, int truncate)
{
	if (truncate > 20)
		return g_strdup_printf ("%.*s...%s", truncate - 10, text, text + strlen(text) - 7);
	else if (truncate > 3)
		return g_strdup_printf ("%.*s...", truncate - 3, text);
	else
		return g_strdup_printf ("%.*s", truncate, text);
}

GtkWidget *
pixmap_menu_item_new (const char *text, const GdkPixbuf *icon, int truncate)
{
	GtkWidget *item;

	item = gtk_pixmap_menu_item_new ();

	if (icon && gnome_preferences_get_menus_have_icons ()) {
		GtkWidget *pixmap;
		GdkPixmap *pixmap_return;
		GdkBitmap *mask_return;
		
		gdk_pixbuf_render_pixmap_and_mask ((GdkPixbuf *) icon, &pixmap_return, &mask_return, 10);
		pixmap = gtk_pixmap_new (pixmap_return, mask_return);

		if(pixmap) {
			gtk_widget_show (pixmap);
			gtk_widget_set_usize (pixmap, ICON_SIZE, ICON_SIZE);
			gtk_pixmap_menu_item_set_pixmap
				(GTK_PIXMAP_MENU_ITEM (item), pixmap);
		}
	}

	if (text) {
		const char *truncated;
		char *string_copy;
		GtkWidget *label;
		int str_length;

		str_length = strlen (text);

		if (truncate > 0 && str_length > truncate) {
			char *tip = strdup (text);
			gtk_tooltips_set_tip(GTK_TOOLTIPS (fma->menu_tips), item,
					     tip, tip);
			string_copy = do_truncate (text, truncate);
			truncated = string_copy;
		} else {
			truncated = text;
			string_copy = NULL;
		}

		label = gtk_label_new (truncated);
		gtk_object_set_data (GTK_OBJECT(item), "label", strdup (truncated));
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_container_add (GTK_CONTAINER (item), label);
		gtk_widget_show (label);

		g_free (string_copy);
	}

	return item;
}

void
remove_old_menus (GtkWidget *item, gpointer menu)
{
	if (GTK_IS_MENU (GTK_MENU_ITEM (item)->submenu)) {
		gtk_container_foreach (GTK_CONTAINER (GTK_MENU_ITEM (item)->submenu), 
				       remove_old_menus, 
				       GTK_MENU_ITEM (item)->submenu);
	}

	if (GTK_IS_MENU_ITEM (item)) {
		gtk_widget_hide (item);
		gtk_container_remove (GTK_CONTAINER (menu), item);
		/* FIXME: We need to remove the label, and the pixmap. */
	}
}

void 
exec_terminal_wrap (GtkWidget *widget, void *path)
{
	gchar *p = path;

	exec_terminal (p);
}

void 
exec_fm_wrap (GtkWidget *widget, void *path)
{
	gchar *p = path;

	exec_application ("nautilus", p, p, FALSE);
}

void 
exec_document_wrap (GtkWidget *widget, void *application_name)
{
	gchar *file_path;
	GnomeVFSMimeApplication *app_name = application_name;
	GdkEventButton *event = (GdkEventButton*)gtk_get_current_event();

	file_path = (gchar *) gtk_object_get_data (GTK_OBJECT (widget), "full_path");	

	if (event->button == 1) {
		if (!app_name) {
			exec_document (file_path);
		} else {
			gtk_signal_emit_by_name (GTK_OBJECT (fma->documents_item->parent), "deactivate", NULL);
			exec_application (app_name->command, file_path, app_name->name, app_name->requires_terminal);
		}
	}
}

void
create_new_directory_menu (directory_menu *dm)
{
	GtkWidget *label;
	GtkWidget *sep;
	GtkWidget *dirtitle;
	
	/* We need to deallocate the old menu before we can regenerate it. */
	gtk_container_foreach (GTK_CONTAINER (dm->dmenu), remove_old_menus, dm->dmenu);
	dm->tearoff = gtk_tearoff_menu_item_new ();

	dirtitle = title_item_new();
	if (strlen (dm->name) > 40)
		label = gtk_label_new (do_truncate (dm->name, 40));
	else
		label = gtk_label_new (dm->name);
	gtk_misc_set_alignment (GTK_MISC(label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_container_add (GTK_CONTAINER (dirtitle), label);
	gtk_object_set_data (GTK_OBJECT (dirtitle), "full_path", (gpointer) dm->name);
	set_drag_stuff_on_directory_menu (dirtitle, dm->name);
	gtk_signal_connect(GTK_OBJECT(dirtitle), "button_press_event",
			   GTK_SIGNAL_FUNC(maybe_popup_dir_menu),
			   dm->dmenu);
	
	sep = gtk_menu_item_new();
	gtk_widget_show(sep);
	gtk_widget_set_sensitive(sep, FALSE);
	
	gtk_widget_show (dm->tearoff);
	gtk_widget_show (dirtitle);
	gtk_menu_append (GTK_MENU (dm->dmenu), dm->tearoff);
	gtk_menu_append (GTK_MENU (dm->dmenu), dirtitle);
	gtk_menu_append (GTK_MENU (dm->dmenu), sep);

#if 0
	{
		char *temp;
		dm->multimenu = q_multi_menu_new ();
		temp = g_strconcat (dm->name, "/", NULL);
		q_multi_menu_set_prefix (dm->multimenu, temp);
		g_free (temp);
	}
#endif
	
	return;
}

void
show_directory_menu (GtkWidget *menu, void *path)
{
	directory_menu *dm;
	GnomeVFSResult result;
	gchar *rp = (gchar *) path;
	gint items;
	GnomeVFSFileInfoOptions options;
	/*	GdkCursor* cursor;*/

	GnomeVFSFileInfo *dir_info;

	dir_info = gnome_vfs_file_info_new ();
	if (rp && rp[0] == '/' && rp[1] == '/')
		rp = rp+1;
	result = gnome_vfs_get_file_info (rp, dir_info, (GNOME_VFS_FILE_INFO_GET_MIME_TYPE
						   | GNOME_VFS_FILE_INFO_FOLLOW_LINKS));
	if (result != GNOME_VFS_OK) {
		fprintf (stderr, "file_menu_applet: %s: %s (%d)\n",
			 (char *)rp, gnome_vfs_result_to_string (result), result);
		return;
	}
	
	dm = (directory_menu *) g_hash_table_lookup (fma->directories, menu);

	if (!dm) {
		dm = g_new (directory_menu, 1);
		dm->mtime = -1;
		dm->name = strdup (rp);
	}

	if ((int) dir_info->mtime == (int) dm->mtime) {
		return;
	} else if (dm->mtime != 0) {
		create_new_directory_menu (dm);
		dm->mtime = dir_info->mtime;

		/* Maybe there are events waiting.  This will make it seem to multitask.*/
		while (gtk_events_pending ()) gtk_main_iteration ();  /* This should no longer be needed. */

		/* We might have already generated this menu in another instance. */
		if (dm->mtime != dir_info->mtime)
			return;
	} else {
		dm->mtime = dir_info->mtime;
	}

	gnome_vfs_file_info_unref (dir_info);

	/*
	cursor = gdk_cursor_new(GDK_CLOCK);
	gdk_window_set_cursor(GTK_WIDGET (dm->dmenu)->window, cursor);
	gdk_cursor_destroy(cursor);
	*/

	/*g_assert (dm->timer == NULL);*/

	dm->timer = g_timer_new ();
	g_timer_start (dm->timer);
	g_timer_elapsed (dm->timer, &dm->last_time);

	if (fma->icon_menus) {
		options = GNOME_VFS_FILE_INFO_FOLLOW_LINKS
			| GNOME_VFS_FILE_INFO_FORCE_FAST_MIME_TYPE
			| GNOME_VFS_FILE_INFO_GET_MIME_TYPE;
	} else {
		options = GNOME_VFS_FILE_INFO_FOLLOW_LINKS
			| GNOME_VFS_FILE_INFO_GET_MIME_TYPE;
	}

	result = gnome_vfs_directory_visit 
		(rp, options,
		 NULL, GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK,
		 (GnomeVFSDirectoryVisitFunc) load_directory_contents,
		 dm);

	g_timer_stop (dm->timer);
	g_timer_destroy (dm->timer);
	dm->timer = NULL;

	gtk_menu_reposition (GTK_MENU (dm->dmenu));    /* The new items could have overextended the menu. */

	items = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (dm->dmenu), "length"));

	if (!items) {
		GtkWidget *menuitem; 
		menuitem = pixmap_menu_item_new ("Empty", NULL, 40);

		gtk_widget_set_sensitive (menuitem, FALSE);

		gtk_widget_show (menuitem);
		gtk_object_set_data (GTK_OBJECT (menuitem), "type", GINT_TO_POINTER (2));
		gtk_object_set_data (GTK_OBJECT (menuitem), "parent", (gpointer) dm->dmenu);
		
		scroll_menu_insert_sorted (GTK_MENU (dm->dmenu), menuitem);
	}

	/* gdk_window_set_cursor(GTK_WIDGET (dm->dmenu)->window, NULL); */

#if 0
	{
		int count = q_multi_menu_count (dm->multimenu);
		int i;
		printf ("Menu count: %d\n", q_multi_menu_count (dm->multimenu));
		for (i = 0; i < count; i++) {
			int j, list_count;
			list_count = q_multi_menu_list_length (dm->multimenu, i);
			printf ("\t%s: %d\n", q_multi_menu_list_label (dm->multimenu, i), list_count);
			for (j = 0; j < list_count; j++) {
				printf ("\t\t%s\n",
					q_multi_menu_list_nth (dm->multimenu, i, j));
			}
		}
	}
#endif

	return;
}

static gboolean
load_directory_contents (const gchar *rel_path, 
			 GnomeVFSFileInfo *file_info,
			 gboolean recursing_will_loop,
			 gpointer data, 
			 gboolean *recurse) {
	directory_menu *dm = (directory_menu *) data;
	gchar *full_path;
	gint items;
	gulong current_time;

	*recurse = FALSE;

	if (file_info->name[0] == '.') {
		return TRUE;        /* We ignore dot-files for now. FIXME: use nautilus prefs? */
	}
	
	items = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (dm->dmenu), "length"));
	gtk_object_set_data (GTK_OBJECT (dm->dmenu), "length", GINT_TO_POINTER (++items));

	/* Assemble the full path name. */
	full_path = g_strdup_printf ("%s/%s", (char *) dm->name, (char *) file_info->name);

#if 0
	q_multi_menu_append (dm->multimenu, 1, (const char **) &full_path);
#endif

	switch (file_info->type) {
	case GNOME_VFS_FILE_TYPE_REGULAR:
		
		/* FILE */
		create_new_file_menuitem (file_info, dm, full_path, (char *)file_info->mime_type);
		break;
		
	case GNOME_VFS_FILE_TYPE_DIRECTORY:
		
		/* DIRECTORY */
		create_new_directory_submenu (file_info, dm, full_path);
		break;
	default:
		break;
	}

	g_timer_elapsed (dm->timer, &current_time);
	if (current_time < dm->last_time ||
	    current_time > dm->last_time + 100 * 1000) {
		gtk_menu_reposition (GTK_MENU (dm->dmenu));    /* The new items could have overextended the menu. */
		/* Maybe there are events waiting.  This will make it seem to multitask.*/
		while (gtk_events_pending ()) gtk_main_iteration ();  /* This should no longer be needed. */
		dm->last_time = current_time;
	}

	/* 	gnome_vfs_file_info_unref (file_info); */
	
	return TRUE;
}

void 
create_new_directory_submenu (GnomeVFSFileInfo *item, directory_menu *dm, char *full_path)
{
	GtkWidget *menuitem;
	GtkWidget *mmenu;

	directory_menu *dm_new;

	menuitem = pixmap_menu_item_new (item->name, flist_mime_icon_load (full_path, item->type, NULL), 40);
	gtk_object_set_data (GTK_OBJECT (menuitem), "type", GINT_TO_POINTER (1));
	gtk_object_set_data (GTK_OBJECT (menuitem), "parent", (gpointer) dm->dmenu);

	mmenu = scroll_menu_new ();
	
	gtk_widget_show (menuitem);
	
	dm_new = g_new (directory_menu, 1);
	dm_new->name = g_strdup (full_path);
	dm_new->mtime = -1;
	dm_new->dmenu = mmenu;
	dm_new->tearoff = NULL;
	dm_new->timer = NULL;
	dm_new->last_time = 0;
	
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), mmenu);
	scroll_menu_insert_sorted (GTK_MENU (dm->dmenu), menuitem);
	
	g_hash_table_insert (fma->directories, menuitem, dm_new);
	
	gtk_signal_connect (GTK_OBJECT (menuitem), "button_press_event", 
			    GTK_SIGNAL_FUNC (maybe_popup_menu), (gpointer) dm->dmenu);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate", 
			    GTK_SIGNAL_FUNC (show_directory_menu), (gpointer) full_path); 
	
	gtk_object_set_data (GTK_OBJECT (menuitem), "full_path", (gpointer) full_path);

	set_drag_stuff_on_directory_menu (menuitem, (gchar *) full_path);
	
}

void 
create_new_file_menuitem (GnomeVFSFileInfo *item, directory_menu *dm, char *full_path, char *mime_type)
{
	GtkWidget *menuitem;

	/*	menuitem = pixmap_menu_item_new (item->name, gicon_get_icon_for_file_scaled 
		(full_path, 15, 15, TRUE));*/
	menuitem = pixmap_menu_item_new (item->name, flist_mime_icon_load 
					 (full_path, item->type, mime_type), 40);
	gtk_object_set_data (GTK_OBJECT (menuitem), "type", GINT_TO_POINTER (2));
	gtk_object_set_data (GTK_OBJECT (menuitem), "full_path", (gpointer) full_path);
	gtk_object_set_data (GTK_OBJECT (menuitem), "parent", (gpointer) dm->dmenu);
	
	gtk_signal_connect (GTK_OBJECT (menuitem), "button_press_event", 
			    GTK_SIGNAL_FUNC (maybe_popup_menu), (gpointer) dm->dmenu);
	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			    GTK_SIGNAL_FUNC (exec_document_wrap), NULL);
	
	gtk_widget_show (menuitem);
	/* gtk_menu_append (GTK_MENU (dm->dmenu), menuitem);*/
	scroll_menu_insert_sorted (GTK_MENU (dm->dmenu), menuitem);

	set_drag_stuff_on_file_menu (menuitem, (gchar *) full_path);
	
}

static ORBit_MessageValidationResult
accept_all_cookies (CORBA_unsigned_long request_id,
		    CORBA_Principal *principal,
		    CORBA_char *operation)
{
	/* allow ALL cookies */
	return ORBIT_MESSAGE_ALLOW_ALL;
}

gint main(gint argc, gchar * argv[])
{
	GtkWidget *tearoff;
	GtkWidget *documents_menu;  
	gchar *sd;
	guint n1, n2, n3;
	guint handlerid;

	GError* error = NULL;
	
	/* init all the services we are using. */
	glade_gnome_init();
	applet_widget_init(PACKAGE, VERSION, argc, argv, NULL, 0, NULL);
	fma = g_new0 (file_menu_applet, 1);
	fma->directories = g_hash_table_new (NULL, NULL);

	if (!gnome_vfs_init ()) {
		fprintf (stderr, "file_menu_applet: Cannot initialize the GNOME Virtual File System.\n");
		return 1;
	}
	
	if (!gconf_init(argc, argv, &error)) {
		g_assert(error != NULL);
		g_warning("GConf init failed:\n  %s", error->message);
		g_error_free(error);
		error = NULL;
		return 1;
	}
	
	/* Shut ORBit up. */
	ORBit_set_request_validation_handler (accept_all_cookies); 

	/* Get Preferences and Defaults */
	fma->applet = applet_widget_new(PACKAGE);
	fma->gconf_key = g_strconcat (GCONF_KEY, NULL, APPLET_WIDGET(fma->applet)->privcfgpath, NULL);
	fma->gconf_key_menu = g_strconcat (fma->gconf_key, "menu_title", NULL);
	fma->gconf_key_dir = g_strconcat (fma->gconf_key, "directory_to_watch", NULL);
	fma->gconf_key_icon = g_strconcat (fma->gconf_key, "icon_lookup", NULL);

	fma->client = gconf_client_get_default();
	gconf_client_add_dir(fma->client, fma->gconf_key, GCONF_CLIENT_PRELOAD_NONE, NULL);
	
	fma->menu_label = gconf_client_get_string (fma->client, fma->gconf_key_menu, NULL);
	fma->dir_watch = gconf_client_get_string (fma->client, fma->gconf_key_dir, NULL);
	fma->icon_menus = gconf_client_get_bool (fma->client, fma->gconf_key_icon, NULL);
	
	fma->icon_theme = gconf_client_get_string (fma->client, NAUTILUS_KEY N_PREF_THEME, NULL);
	nautilus_theme = fma->icon_theme;

	if (!fma->menu_label)
		fma->menu_label = strdup ("Home Directory");

	/* Create */
	fma->menu_bar = gtk_menu_bar_new ();
	fma->documents_item = gtk_menu_item_new_with_label (fma->menu_label);
	documents_menu = scroll_menu_new ();
	tearoff = gtk_tearoff_menu_item_new();
	fma->menu_tips = gtk_tooltips_new();
	fma->dm = g_new (directory_menu, 1);
	sd = translate_directory_name (fma->dir_watch);

	gtk_object_set_data(GTK_OBJECT(fma->applet), "client", fma->client);
	
	/* Configure */
	gtk_menu_bar_set_shadow_type (GTK_MENU_BAR (fma->menu_bar), GTK_SHADOW_NONE);
	

	/* Preferences redux */
	n1 = gconf_client_notify_add(fma->client, fma->gconf_key_menu, 
				     set_menu_title, fma->documents_item, NULL, NULL);
	n2 = gconf_client_notify_add(fma->client, fma->gconf_key_dir, 
				     change_watched_directory, NULL, NULL, NULL);
	n3 = gconf_client_notify_add(fma->client, fma->gconf_key_icon, 
				     change_icon_setting, NULL, NULL, NULL);

	/* Pack */
	applet_widget_add(APPLET_WIDGET(fma->applet), fma->menu_bar);
	gtk_menu_bar_append (GTK_MENU_BAR (fma->menu_bar), fma->documents_item);
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (fma->documents_item), documents_menu);
	gtk_menu_append (GTK_MENU (documents_menu), tearoff);
	fma->dm->name = sd;
	fma->dm->mtime = -1;
	fma->dm->dmenu = documents_menu;
	fma->dm->tearoff = tearoff;
	
	/* Display */
	gtk_widget_show (fma->menu_bar);
	gtk_widget_show (fma->documents_item);
	gtk_widget_show (fma->applet);
	gtk_tooltips_enable (fma->menu_tips);
	g_hash_table_insert (fma->directories, fma->documents_item, fma->dm);

	/* Callbacks */

	set_drag_stuff_on_menu_bar (fma->documents_item);

        applet_widget_register_stock_callback((AppletWidget *) fma->applet,
                                              "Properties",
                                              GNOME_STOCK_MENU_PROP,
                                              "Properties", show_preferences_menu,
                                              (gpointer) fma->applet);
	applet_widget_register_stock_callback((AppletWidget *) fma->applet,
					      "About...",
					      GNOME_STOCK_MENU_ABOUT,
					      "About...", doabout, NULL);
	handlerid = gtk_signal_connect (GTK_OBJECT (fma->documents_item), 
					"activate", 
					GTK_SIGNAL_FUNC (show_directory_menu), 
					(gpointer) sd);
	
	/* We need to remember the handler so we can remove it again. */
	gtk_object_set_data (GTK_OBJECT (fma->documents_item), "handler", (gpointer) handlerid);
	gtk_object_set_data (GTK_OBJECT (fma->documents_item), "dm", (gpointer) fma->dm);

	/* cleanup and exit */
	gtk_main();

	/* So we need to detach cleanly from gconf.  Ugh. */
	gconf_client_notify_remove (fma->client, n1);
	gconf_client_notify_remove (fma->client, n2);
	gtk_object_unref(GTK_OBJECT(fma->client));

	return 0;
}

